function annotateTargetSum(S,fig,ax,varargin)
% annotateTargetSum draws an arrow into graphs created by drawNetwork at
% the position corresponding to the target sum
%
%   SUBSUMNETWORKCLASS.ANNOTATETARGETSUM(fig,ax)
%   draws an arrow into the figure fig (created by drawNetwork) at
%   the position of the axes object ax corresponding to the target sum
%   defined in SUBSUMNETWORKCLASS.TargetSum
%
%   Configuration Parameters:
%     FontSize: Font size of the number, default: 12
%     DrawNumber: Defines whether the number should be drawn, default: true
%     YScale: Defines how large the arrow should be, default: 0.75
%
%   See also DRAWNETWORK, SubSumNetworkClass

p=inputParser;
p.addParameter('FontSize',12,@isnumeric); % Font size of the number
p.addParameter('DrawNumber',true,@islogical); % Defines whether the number should be drawn
p.addParameter('YScale',0.75,@isnumeric); % Defines how large the arrow should be
p.parse(varargin{:});
if ~isempty(S.TargetSum)
  %create an arrow pointing at the targetSum
  axPos = get(ax,'position');
  labh = get(ax,'XLabel');
  set(labh, 'Units','normalized');
  labelPos = get(labh,'Position');
  set(labh, 'Units','data');
  xlimits = get(ax, 'XLim');
  relPos = axPos(3) * (S.TargetSum - xlimits(1)) / (xlimits(2) - xlimits(1));
  xPos = S.checkPos([axPos(1) + relPos, axPos(1) + relPos]);
  yPos = S.checkPos([axPos(2) + labelPos(2) * p.Results.YScale, axPos(2)]);
  if p.Results.DrawNumber
    annotation(fig, 'textarrow', xPos, yPos, 'String', num2str(S.TargetSum), ...
      'Color', [1 0 0], 'FontSize', p.Results.FontSize);
  else
    annotation(fig, 'arrow', xPos, yPos, 'Color', [1 0 0]);
  end
end
end
