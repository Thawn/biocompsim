function [fig,ax]=makeplot(S,varargin)
%makeplot creates a plot of the simulation results (agents per exit).
%
% Configuration parameters:
% makeplot('agentName','agents'); used in filename of saved pdf
%
% makeplot('correctErr',sqrt(SubSumNetworkClass.Results(1,:).*SubSumNetworkClass.Valid));
% makeplot('wrongErr',sqrt(SubSumNetworkClass.Results(1,:).*~SubSumNetworkClass.Valid));
% Used for error bars if the plot contains less than 50 bars (otherwise the
% plot would be too crowded).
%
% makeplot('stacked',false); If true, correct exits are displayed as
% stacked bars where agents that made an error during the simulation (and
% thus only arrived at a correct exit by chance) are shown in magenta and
% only agents that did not make any error at all are shown in green.
%
% makeplot('Save',true); Whether the plot should be saved.
% makeplot('AnnotationText',''); A text placed on the plot
% makeplot('Xlimits',[]); Limits for the X axis. Leave empty for
% autoscaling.
% makeplot('Ylimits',[]); Limits for the Y axis. Leave empty for
% autoscaling.
% makeplot('YScale',0.85); If we use Ylimit autoscaling, this factor can be
% used to adjust the height of the bars within the plot.
%
% makeplot('Correct',SubSumNetworkClass.Results(1,:).*SubSumNetworkClass.Valid);
% makeplot('Wrong',SubSumNetworkClass.Results(1,:).*~SubSumNetworkClass.Valid);
% The amount of agents in the correct and wrong exits. By default this is calculated
% from the results in the SubSumNetworkClass object.
%
% makeplot('Axes',[]); The axes object into which the plot is to be drawn.
% makeplot('ForTalk',false); When true, font size and line thickness
% is increased to improve legibility in a presentation.
% makeplot('Aspect',29.7/21); Set the aspect ratio of the plot
% 
% See also SubSumNetworkClass, simulate, multisim

Correct=S.Results(1,:).*S.Valid;
Wrong=S.Results(1,:).*~S.Valid;
p=inputParser;
%define the parameters
p.addParameter('agentName','agents',@ischar);
p.addParameter('correctErr',sqrt(Correct),@isnumeric);
p.addParameter('wrongErr',sqrt(Wrong),@isnumeric);
p.addParameter('stacked',false,@islogical);
p.addParameter('Save',true,@islogical);
p.addParameter('AnnotationText','',@(x)ischar(x) || iscellstr(x) ); %#ok<ISCLSTR>
p.addParameter('TargetArrow',true,@islogical);
p.addParameter('Xlimits',[],@isnumeric);
p.addParameter('Ylimits',[],@isnumeric);
p.addParameter('YScale',0.85,@isnumeric);
p.addParameter('Correct',Correct,@isnumeric);
p.addParameter('Wrong',Wrong,@isnumeric);
p.addParameter('Axes',[],@(x) isempty(x) || ishandle(x));
p.addParameter('ForTalk',false,@islogical);
p.addParameter('Aspect',29.7/21,@isnumeric);
%now parse the parameters
p.parse(varargin{:});

Correct=p.Results.Correct;
Wrong=p.Results.Wrong;

if isempty(S.Valid)
  S.determineCorrectSums;
end
if isempty(S.Results)
  S.Simulate;
end
numResults=length(Correct);
X=S.Offset:S.Offset+numResults-1;
[XLimits, XTicks, XTickLabel] = S.calculateXLimits('Xlimits', p.Results.Xlimits);
XIndices=round(XLimits-S.Offset);
if XIndices(1)<1
  XIndices(1)=1;
end
if XIndices(2)>length(Correct)
  XIndices(2)=length(Correct);
end
if isempty(p.Results.Ylimits)
  Ylimits=[0 (max(Correct(XIndices(1):XIndices(2)) + Wrong(XIndices(1):XIndices(2)) + p.Results.correctErr(XIndices(1):XIndices(2)) + p.Results.wrongErr(XIndices(1):XIndices(2))))*1/p.Results.YScale];
else
  Ylimits = p.Results.Ylimits;
end
if Ylimits(2)==0
  Ylimits=[0 1];
end
if isempty(p.Results.Axes)
  if p.Results.ForTalk
    Width=15;
  else
    Width=29.7;
  end
  fig=createBasicFigure('Width',Width,'Aspect',p.Results.Aspect);
  ax=axes('Parent',fig);
else
  ax=p.Results.Axes;
  fig=get(ax,'Parent');
  while ~isempty(fig) & ~strcmp('figure', get(fig,'type')) %#ok<AND2>
    fig = get(fig,'parent');
  end
end
if p.Results.stacked
  h=bar(ax,X,[Wrong' Correct'],'stacked');
  plot1=h(2);
  plot2=h(1);
  edgeColor='none';
else
  plot2=bar(ax,X,Wrong,'m');
  hold(ax,'on');
  plot1=bar(ax,X,Correct,'g');
  edgeColor=[0 0 0];
end
CommonAxesProperties={'XLim', XLimits, 'YLim',Ylimits };
set(plot1,'DisplayName','correct results','FaceColor','g','EdgeColor', edgeColor);
set(plot2,'DisplayName','incorrect results','FaceColor','m','EdgeColor', edgeColor);
legend1=legend([plot1 plot2]);
set(legend1,'Orientation','horizontal','Location','NorthEast','FontSize',12);
ylabel('# of agents','FontSize',14);
xlabel('exit #','FontSize',14);
% Create textbox
if ~isempty(p.Results.AnnotationText)
  annotation(fig,'textbox',...
    [0.180925842002266 0.620266394989058 0.50478844371202 0.137828843106182],...
    'String',p.Results.AnnotationText,...
    'FontSize',12,...
    'LineStyle','none',...
    'Interpreter', 'latex');
end
if ~isempty(S.TargetSum) && p.Results.TargetArrow
  S.annotateTargetSum(fig,ax,'DrawNumber',false);
end
if max(p.Results.correctErr)>0 && XLimits(2) - XLimits(1)<50
  ErrorX=X(XIndices(1):XIndices(2));
  Data=Wrong(XIndices(1):XIndices(2))+Correct(XIndices(1):XIndices(2));
  Errors=p.Results.wrongErr(XIndices(1):XIndices(2))+p.Results.correctErr(XIndices(1):XIndices(2));
  hold(ax,'on');
  errorbar(ErrorX,Data,Errors,'Parent',ax,'LineStyle','none','Color','k');
end
set(ax, CommonAxesProperties{:}, 'XTick', XTicks, 'XTickLabel', XTickLabel, 'Box', 'off', ...
  'FontSize', 12, 'LineWidth', 0.75);

hold(ax,'off');
if p.Results.Save
  saveas(fig,fullfile(S.SavePath, [ellipsize( [inputname(1) '_makeplot(' p.Results.agentName ')'], 251) '.pdf']));
end
