function [fig,ax,AgentName]=drawNetwork(S,varargin)
%drawNetwork draws all legal paths through the subset sum network.
%
% Configuration parameters:
%   drawNetwork('AnnotationText',{}); Cellstring containing an 
%   Annotation text placed on the figure.
%   drawNetwork('AnnotationPos',[0.2 0.3 0.6 0.6]); Position
%   vector for the annotation text.
%   drawNetwork('ForTalk',false); When true, font size and
%   line thickness is increased to improve legibility in a presentation.
%   drawNetwork('Aspect',29.7/21); Aspect ratio of the figure.
%   drawNetwork('drawResetFalse',false); Whether to draw ResetFalse
%   junctions.
%   drawNetwork('Save',true); Whether the figure should be
%   saved as pdf.
%   drawNetwork('Axes',[]); Can be used to pass
%   an existing axes object into which the network will be drawn.
%   drawNetwork('Figure',[]); Can be used
%   to pass an existing figure into which the network will be drawn.
%
% See also SubSumNetworkClass

p=inputParser;
p.addParameter('AnnotationText',{},@iscell);
p.addParameter('AnnotationPos',[0.2 0.3 0.6 0.6],@isnumeric);
p.addParameter('ForTalk',false,@islogical);
p.addParameter('Width', 21, @isnumeric);
p.addParameter('Aspect', S.ProblemSize / S.NumRows * S.UnitCellWidth / S.UnitCellHeight, @isnumeric);
p.addParameter('drawResetFalse',false,@islogical);
p.addParameter('ArrowScale',0.75,@isnumeric);
p.addParameter('Save',true,@islogical);
p.addParameter('Axes',[],@(x) isempty(x) || ishandle(x));
p.addParameter('Figure',[],@(x) isempty(x) || ishandle(x));
p.parse(varargin{:});

AnnotationText=p.Results.AnnotationText;
Numbers=zeros(1,length(S.Numbers)+1);
Numbers(2:end)=S.Numbers;

if isempty(p.Results.Figure)
    Width = p.Results.Width;
    Aspect = p.Results.Aspect;
    if any(strcmpi('Aspect', p.UsingDefaults)) && p.Results.ForTalk
        Width = 15;
        Aspect = 2;
    end
    fig = createBasicFigure('Width', Width, 'Aspect', Aspect);
else
  fig=p.Results.Figure;
end
if isempty(p.Results.Axes)
  ax=axes('Parent',fig);
else
  ax=p.Results.Axes;
  if isempty(p.Results.Figure)
    fig=get(ax,'Parent');
    while ~isempty(fig) & ~strcmp('figure', get(fig,'type')) %#ok<AND2>
      fig = get(fig,'parent');
    end
  end
end
if p.Results.ForTalk
  MarkerSize=2;
else
  MarkerSize=4;
end
if ~p.Results.drawResetFalse
  S.ResetFalseJunctionMatrix=[];
  S.TopHalfSplitJunctionMatrix = [];
  S.ResetTrueJunctionMatrix = [];
  S.createNetworkMap;
end

FontSize=10;
set(ax,'FontSize',FontSize,'YTick',zeros(1,0),'YTickLabel',{},'Units','normalized');
if ~isempty(S.Entrances)
  hold(ax,'on');
  Junction = DrawNetworkJunctionClass(ax, sum(Numbers), S.Offset, 'MarkerSize', MarkerSize);
  S.runSim(Junction, Junction.createLineRegister(S.ProblemSize));
  
  %now we do some layouting
  axPos=get(ax,'position');
  [XLimits, XTicks, XTickLabel] = S.calculateXLimits;
  xlim(ax,XLimits);
  set(ax, 'XTick', XTicks, 'XTickLabel', XTickLabel);
  if ~isempty(S.TargetSum) && p.Results.ArrowScale > 0
    %create an arrow pointing at the targetSum
    S.annotateTargetSum(fig,ax,'FontSize',FontSize, 'YScale', p.Results.ArrowScale);
  end
  scale=ylim(ax);
  scale=scale(2)-scale(1);
  labh = get(ax,'XLabel');
  set(labh,'Units','normalized');
  labelPos=get(labh,'Position');
  set(labh,'Units','data');
  y=(sum(Numbers))/scale*axPos(4);
  yPos=S.checkPos([y y]+axPos(2));
  xPos=S.checkPos([axPos(1)+labelPos(2)*0.5 axPos(1)]);
  annotation(fig,'textarrow',xPos,yPos,'String',num2str(sum(Numbers)),...
    'Color',[0 0.58 0.23],'FontSize',FontSize);
  
  runningSum=0;
  for n=2:length(Numbers)
    height=Numbers(n)/scale;
    labelText=num2str(Numbers(n));
    width=0.1;
    runningSum=runningSum+Numbers(n);
    y=(sum(Numbers)-runningSum)/scale*axPos(4);
    pos=[[-width y]+axPos(1:2) width height*axPos(4)];
    if height>0.033
      labelSize=FontSize;     
    else
      labelSize=FontSize*height*30;
    end
    if labelSize < 4
      labelSize = 4;
    end
    pos(pos < 0) = 0;
    pos(pos > 1) = 1;
    annotation(fig,'textbox',pos,'String',labelText,...
      'FontSize',labelSize,'LineStyle','none','HorizontalAlignment','right',...
      'VerticalAlignment','middle');
  end
end
if ~isempty(AnnotationText) && ~p.Results.ForTalk
  annotation(fig,'textbox',p.Results.AnnotationPos,'String',AnnotationText,...
    'FontSize',FontSize,'Interpreter','none','Margin',0,'LineStyle','none');
end
if p.Results.Save
  fieldsToClean={'AnnotationText','numbers','plot','Aspect','Figure','Axes'};
  Temp=struct('numbers',S.Numbers); %#ok<NASGU>
  AgentName=evalc('disp(Temp)');
  AgentName=S.makeAgentName(AgentName,p.Results,unique([p.UsingDefaults, fieldsToClean]));
  saveas(fig,['results' filesep 'drawNetwork(' AgentName ').pdf']);
else
  fig.Visible = 'on';
end
end