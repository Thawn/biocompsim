function [fig,ax,Results,Errors,AgentName]=multisim(S,varargin)
%multisim creates a plot of the simulation results (agents per exit).
%
% Configuration parameters:
% multisim('NumCores',1,@isnumeric);
% multisim('Iterations',10,@isnumeric);
% multisim('Plot',2,@isnumeric);
% multisim('moveResultsFromTo',[],@isnumeric);
% multisim('AnnotationText','',@(x)ischar(x) || iscellstr(x) );
% multisim('Xlimits',[],@isnumeric);
% multisim('Ylimits',[],@isnumeric);
% multisim('YScale',0.85,@isnumeric);
% multisim('Entrances',0,@isnumeric);
% multisim('CorrectResetFalseError',false,@islogical);
% multisim('LogErrors',false,@islogical);
% multisim('Axes',[],@(x) isempty(x) || ishandle(x));
% 
% See also SubSumNetworkClass, simulate, makeplot

p=inputParser;
%define the parameters
p.addParameter('NumCores',1,@isnumeric);
p.addParameter('Iterations',10,@isnumeric);
p.addParameter('Plot',2,@isnumeric);
p.addParameter('moveResultsFromTo',[],@isnumeric);
p.addParameter('AnnotationText','',@(x)ischar(x) || iscellstr(x) );
p.addParameter('Xlimits',[],@isnumeric);
p.addParameter('Ylimits',[],@isnumeric);
p.addParameter('YScale',0.85,@isnumeric);
p.addParameter('Entrances',0,@isnumeric);
p.addParameter('CorrectResetFalseError',false,@islogical);
p.addParameter('LogErrors',false,@islogical);
p.addParameter('Axes',[],@(x) isempty(x) || ishandle(x));
p.addParameter('Save', true, @islogical);
p.addParameter('TargetArrow',true,@islogical);

p.KeepUnmatched=true;

p.parse(varargin{:});
Iterations=p.Results.Iterations;
Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
SimArgs = reshape(Tmp',[],1)';

MoveResultsFromTo=p.Results.moveResultsFromTo;

if p.Results.LogErrors
  S.LogErrors=true;
else
  S.LogErrors=false;
end

if isempty(S.Valid)
  S.determineCorrectSums;
end
Xlimits = S.calculateXLimits('Xlimits',p.Results.Xlimits);
Ylimits=p.Results.Ylimits;
Results=zeros(Iterations,S.ProblemSize);
Errors=zeros(Iterations,S.ProblemSize);
if p.Results.Plot>0
  fprintf('Simulating %d rounds with non-default parameters: %s.\n',Iterations,S.SaveName);
end
if p.Results.NumCores>1
  ClosePool=false;
  if isempty(gcp('nocreate'))
    ClosePool=true;
    Pool=parpool(p.Results.NumCores);
  end
  Plot=p.Results.Plot;
  Temp(Iterations)=S.copyClass;
  for n=1:Iterations-1
    Temp(n)=S.copyClass;
  end
  timer=zeros(1:Iterations);
  parfor i=1:Iterations
    T=Temp(i);
    timer(i)=tic; %#ok<PFOUS>
    T.simulate(SimArgs{:}); %#ok<PFBNS>
    Results(i,:)=T.Results(1,:);
    Errors(i,:)=T.Results(2,:);
    ElapsedTime=toc(timer(i));
    if Plot>0
      fprintf('run completed in %.3f s.\n',ElapsedTime);
    end
    Temp(i)=T;
  end
  S.Results=Temp(end).Results;
  if ClosePool
    delete(Pool);
  end
else
  TotalTime=0;
  for i=1:Iterations
    timer=tic;
    S.simulate(SimArgs{:});
    Results(i,:)=S.Results(1,:);
    Errors(i,:)=S.Results(2,:);
    ElapsedTime=toc(timer);
    TotalTime=TotalTime+ElapsedTime;
    if p.Results.Plot>0
      fprintf('Simulating %d of %d ETR: %.3f.\n',i,Iterations,(Iterations-i)*TotalTime/i)
    end
  end
end
if p.Results.Plot>0
  MakeplotArgs={'Save',p.Results.Plot>1,...
    'AnnotationText',p.Results.AnnotationText,...
    'Xlimits',Xlimits,'Ylimits',Ylimits,...
    'YScale',p.Results.YScale,...
    'Axes',p.Results.Axes,...
    'TargetArrow', p.Results.TargetArrow};
  Stdev=std(Results+Errors);
  M=mean(Results);
  FieldsToClean={'AnnotationText','iterations','Plot','Save'};
  AgentName=S.makeAgentName(S.SaveName,p.Unmatched, FieldsToClean);
  AgentName=S.makeAgentName(AgentName,p.Results,unique([p.UsingDefaults, FieldsToClean]));
  if isempty(AgentName)
    AgentName='allDefaults';
  end
  if ~isempty(S.ResetFalseJunctionMatrix)
    AgentName=[AgentName '_drawResetFalse-1'];
  end
  if ~isempty(S.ResetFalseJunctionMatrix) && p.Results.CorrectResetFalseError
    NoAddJitter=sum(S.ResetFalseJunctionMatrix);
    NoAddJitter=2.^NoAddJitter(S.Offset+1:S.ProblemSize+S.Offset);
    Stdev=Stdev./NoAddJitter;
    M=M./NoAddJitter;
    Empty=zeros(size(NoAddJitter));
    S.makeplot('Correct',Empty,'Wrong',NoAddJitter,...
      'agentName',['NoAddJitter_',AgentName],...
      'wrongErr',Empty,...
      'stacked',false,...
      MakeplotArgs{:});
  end
  if S.LogErrors
    Es=std(Errors);
    Em=mean(Errors);
    if ~isempty(MoveResultsFromTo)
      %test lost triangle hypothesis
      M(MoveResultsFromTo(2))=M(MoveResultsFromTo(2))+M(MoveResultsFromTo(1));
      Stdev(MoveResultsFromTo(2))=sqrt(Stdev(MoveResultsFromTo(2))^2+Stdev(MoveResultsFromTo(1))^2);
      M(MoveResultsFromTo(1))=0;
      Stdev(MoveResultsFromTo(1))=0;
    end
    [fig,ax]=S.makeplot('Correct',M,'Wrong',Em, 'agentName',AgentName,...
      'correctErr',Stdev, 'wrongErr',Es, 'stacked',true,...
      MakeplotArgs{:});
  else
    CorrectS=Stdev.*S.Valid;
    IncorrectS=Stdev.*~S.Valid;
    CorrectM=M.*S.Valid;
    IncorrectM=M.*~S.Valid;
    if ~isempty(MoveResultsFromTo)
      %test lost triangle hypothesis
      CorrectM(MoveResultsFromTo(2))=CorrectM(MoveResultsFromTo(2))+CorrectM(MoveResultsFromTo(1));
      CorrectS(MoveResultsFromTo(2))=sqrt(CorrectS(MoveResultsFromTo(2))^2+CorrectS(MoveResultsFromTo(1))^2);
      CorrectM(MoveResultsFromTo(1))=0;
      CorrectS(MoveResultsFromTo(1))=0;
    end
    [fig,ax]=S.makeplot('Correct',CorrectM,'Wrong',IncorrectM,...
      'agentName',AgentName,...
      'correctErr',CorrectS,'wrongErr',IncorrectS,...
      'stacked',false,...
      MakeplotArgs{:});
  end
  if p.Results.Save>1
    save(fullfile(S.SavePath, [ellipsize( [inputname(1) '_multisim(' AgentName ')'], 251) '.mat']),'Results','Errors');
  end
end
