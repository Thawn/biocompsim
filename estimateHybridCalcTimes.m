function [HybridTimes,CompCardinality,StepsLog,TimesLog] = estimateHybridCalcTimes(varargin)
p=inputParser;
p.addParameter('Numbers', primes(173), @isnumeric);
p.addParameter('FilamentType', 'microtubule', @isnumeric);
p.addParameter('DiagonalUnitLength', [], @isnumeric);
p.addParameter('FilamentSpeed', [], @isnumeric);
p.addParameter('Resolution', 0.1, @isnumeric);
p.addParameter('TimeUnit', 'h', @ischar);
p.addParameter('Aspect', 4/3, @isnumeric);
p.addParameter('CompTimesMeasured', [], @isnumeric);
p.addParameter('ResultsPath', 'results', @ischar);
p.parse(varargin{:})
switch p.Results.FilamentType
  case 'actin'
    DiagonalUnitLength = 5.415; %for the actin device, for the MT device it is 10.6398
    FilamentSpeed = 5; %in �m/s; default is set for actin
  case 'microtubule'
    DiagonalUnitLength = 10.6398;
    FilamentSpeed = 1;
end
if ~isempty(p.Results.DiagonalUnitLength)
  DiagonalUnitLength = p.Results.DiagonalUnitLength;
end
if ~isempty(p.Results.FilamentSpeed)
  FilamentSpeed = p.Results.FilamentSpeed;
end
switch p.Results.TimeUnit
  case 'm'
    TimeCor=60;
  case 'h'
    TimeCor=3600;
  otherwise
    TimeCor=1;
end
Numbers=sort(p.Results.Numbers);
Cardinality=length(Numbers);
[CompTimes,CompCardinality]=extrapolateComputingTime(Cardinality,...
  'Resolution',1,'CompTimesMeasured', p.Results.CompTimesMeasured);
NumMeasured = length(p.Results.CompTimesMeasured);
if NumMeasured > 0
  CompTimes(1:NumMeasured) = p.Results.CompTimesMeasured;
else
  NumMeasured = 25;
end  
BioTimes = zeros(1,Cardinality-1);
for n = 1:Cardinality-1
  BioTimes(n) = sum(Numbers(1:n+1))*DiagonalUnitLength/FilamentSpeed;
end
[InterpCompTimes,InterpCompCardinality]=extrapolateComputingTime(Cardinality);
NextCompStepTimes=CompTimes(2:end)-CompTimes(1:end-1);
MinBiotTime=DiagonalUnitLength*Numbers(1)/FilamentSpeed;
HybridTimes=CompTimes(NextCompStepTimes<MinBiotTime);
HybridTimes=[HybridTimes CompTimes(length(HybridTimes)+1)];
TimesLog=zeros(2,Cardinality-1);
StepsLog=zeros(2,Cardinality-1);
CompSteps=length(HybridTimes)+1;
TimesLog(1,1:CompSteps-1)=HybridTimes;
StepsLog(1,1:CompSteps-1)=2:CompSteps;
BioSteps=1;
for i=CompSteps:Cardinality-1
  TimeComp=CompTimes(CompSteps-1);
  TimeBio=sum(Numbers(1:BioSteps))*DiagonalUnitLength/FilamentSpeed;  
  NextBioStepTime=Numbers(BioSteps+1)*DiagonalUnitLength/FilamentSpeed;
  StepsLog(1,i)=CompSteps;
  StepsLog(2,i)=BioSteps;
  TimesLog(1,i)=TimeComp;
  TimesLog(2,i)=TimeBio;
  HybridTimes(i)=TimeComp+TimeBio;
  if NextBioStepTime<NextCompStepTimes(CompSteps-1)
    BioSteps=BioSteps+1;
  else
    CompSteps=CompSteps+1;
  end
end
Fig1 = createBasicFigure('Width',15,'Aspect',p.Results.Aspect);
Fig1.Visible='on';
Ax = gca;
Ax.FontSize = 12;
plot(2:NumMeasured+1,CompTimes(1:NumMeasured)/TimeCor,'ro','MarkerSize',4,'DisplayName','computing time (measured)');
hold on;
plot(InterpCompCardinality,InterpCompTimes/TimeCor,'r-','DisplayName','computing time (extrapolated)');
plot(2:Cardinality,BioTimes/TimeCor,'kv','DisplayName',sprintf('%s travel time (estimated)',p.Results.FilamentType));
plot(2:Cardinality,HybridTimes/TimeCor,'bs','DisplayName','hybrid computing time (estimated)');
ylim(Ax,[0 10]/(3600/TimeCor));
ylabel(Ax,sprintf('computing time (%s)',p.Results.TimeUnit),'FontSize',14);
xlabel(Ax,'cardinality (number of primes)','FontSize',14);
Lgd = legend(Ax,'Location','best');
Lgd.FontSize = 12;
saveas(Fig1,fullfile(p.Results.ResultsPath,sprintf('estHybridCalcTimes(Agent_%s,Card_%d).pdf',p.Results.FilamentType,Cardinality)),'pdf');
