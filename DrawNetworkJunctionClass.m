classdef DrawNetworkJunctionClass < JunctionClass
  %DrawNetworkJunctionClass class used to quickly find the correct exits
  %of a subset sum network. And in addition save the coordinates needed to
  %draw the correct paths through the network.
  %
  % See also: SubSumNetworkClass,JunctionClass,LogErrorJunctionClass,CorrectedJunctionClass,CorrectedLogErrorJunctionClass
  
  properties
    Ax; %Axes handle where the network should be plotted
    NumRows; %total number of rows in the network
    CurRow; %current network row
    Offset; %offset of the network columns
    LineSpec = 'b-';
    MarkerSize = 4;
    PassJunctionSpec = 'y.';
    PassJunctionScale = 0.5;
    SplitJunctionSpec = 'co';
    SplitJunctionScale = 1;
    SplitTopJunctionSpec = 'bo';
    SplitTopJunctionScale = 1;
    SplitLeftJunctionSpec = 'go';
    SplitLeftJunctionScale = 1;
    ResetFalseJunctionSpec = 'rv';
    ResetFalseJunctionScale = 1;
    ResetTrueJunctionSpec = 'm>';
    ResetTrueJunctionScale = 1;
  end
  methods
    
    
    function J=DrawNetworkJunctionClass(Ax, NumRows, Offset, varargin)
      if nargin > 0
        p=inputParser;
        p.addRequired('Ax',@ishandle);
        p.addRequired('NumRows', @isnumeric);
        p.addRequired('Offset', @isnumeric);
        p.KeepUnmatched=true;
        p.parse(Ax, NumRows, Offset, varargin{:});
        Names=fieldnames(p.Unmatched);
        for i=1:length(Names)
          if isempty(J.findprop(Names{i}))
            warning(strcat('DrawNetworkJunctionClass does not support the parameter: ', Names{i}));
          else
            J.(Names{i})=p.Unmatched.(Names{i});
          end
        end
        J.Ax = p.Results.Ax;
        J.NumRows = p.Results.NumRows;
        J.CurRow = J.NumRows;
        J.Offset = p.Results.Offset;
        hold(J.Ax, 'on');
      end
    end
    
    
    function [Register, J]=calculateLineSegment(J, Register, ind, JunctionType)
      %calculateLineSegment calculates the behavior of agents for the 
      %current part of a line of a subset sum network.
      %
      % See also: SubSumNetworkClass.simulate
      n = ind * 2 - 1;
      [one, two] = J.calculate(Register(1, n), Register(1, n + 1), JunctionType);
      Register(2, n) = one;
      Register(2, n + 3) = two;
      XStart = J.Offset + ind - 1;
      X = [];
      Y = [];
      if Register(2, n)
        %plot vertical line
        X = [ XStart; XStart ];
        Y = [J.CurRow; J.CurRow - 1];
      end
      if Register(2, n + 3)
        %plot diagonal line
        X = [X, [XStart; XStart + 1]];
        Y = [Y, [J.CurRow; J.CurRow - 1]];
      end
      plot(J.Ax, X, Y, J.LineSpec)
      plot(J.Ax, XStart, J.CurRow, J.(sprintf('%sJunctionSpec', JunctionType)), 'MarkerSize', J.MarkerSize * J.(sprintf('%sJunctionScale', JunctionType)));
    end
    
    
    function [Register, J] = shiftLineRegister(J, Register)
      %shiftLineRegister after processing a line is complite, this function
      %shifts the lines of a register sucht that processing of the next
      %line can begin.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register(3, : ) = DrawNetworkJunctionClass.insertCoordinates(Register(1, :));
      Register(4, : ) = DrawNetworkJunctionClass.insertCoordinates(Register(2, :));
      Register(3, 4:2:size(Register, 2)) = Register(3, 2:2:size(Register, 2) - 2); %shift input x coordinates such that they are in the same column as the output x coordinates
      Register(3, 2) = NaN;
      Register(1, :) = Register(2, :);
      Register(2, :) = false;
      J.CurRow = J.CurRow - 1;
    end
    
    
  end
  methods (Static)
    
    
    function [Output1, Output2] = calculate(InputB, InputA, JunctionType) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate calculates the behavior of agents in a junction. 
      %
      % Parameters:
      %   JunctionType: string, name of the junction, part of the function
      %   that will be called for the actual calculation.
      %   InputB: number of agents entering from the top.
      %   InputA: number of agents entering from the left.
      %
      % See also: calculateLineSegment, monteCarlo
      
      if nargin < 3
        JunctionType = 'Pass';
      end
      switch JunctionType
        case 'Split'
          Output1 = InputB | InputA;
          Output2 = Output1;
        case 'SplitTop'
          Output1 = InputB;
          Output2 = InputB | InputA;
        case 'SplitLeft'
          Output1 = InputB | InputA;
          Output2 = InputA;
        case 'ResetFalse'
          Output1 = InputB | InputA;
          Output2 = 0;
        case 'ResetTrue'
          Output1 = 0;
          Output2 = InputB | InputA;
        otherwise % Pass junction is the default
          Output1 = InputB;
          Output2 = InputA;
      end
    end
    
    
    function Register=createLineRegister(ProblemSize)
      %createLineRegister create a register of the appropriate dimension
      %and type for this junction class.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register = zeros(4, (ProblemSize + 1) * 2, 'single');
      Register(3:4, :) = NaN(2, (ProblemSize + 1) * 2);
    end
    
    
    function Output = insertCoordinates(Register)
      %insertCoordinates output a Matrix of the same size as Retister but
      %with ones replaced by their respective column number and zeros by NaN.
      %
      % See also: calculateLine
      
      Output = NaN(size(Register));
      Output(Register ~= 0) = ceil(find(Register) / 2) - 1;
    end
    
    
  end
end