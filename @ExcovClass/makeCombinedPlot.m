function [Fig, Axes, AgentName] = makeCombinedPlot(E, varargin)
p=inputParser;
p.addParameter('AnnotationText',{},@iscell);
p.addParameter('ForTalk',false,@islogical);
p.addParameter('Aspect',2,@isnumeric);
p.addParameter('drawResetFalse',true,@islogical);
p.addParameter('Save',true,@islogical);
p.addParameter('Optimized',true,@islogical);
p.addParameter('Sort', 'descend', @ischar);
p.addParameter('Experiment', [], @(x) isnumeric(x) || iscell(x));
p.KeepUnmatched=true;
p.parse(varargin{:});
Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
Passthrough = [{'Optimized', p.Results.Optimized, 'Sort', p.Results.Sort} reshape(Tmp', [], 1)'];
if ~p.Results.drawResetFalse
  Passthrough=[{'SimulateResetFalse', false, 'ResetNetwork', false} Passthrough];
end
DrawResults=p.Results;
FieldsToClean={'Save','Experiment'};
for n=1:length(FieldsToClean)
  DrawResults=rmfield(DrawResults,FieldsToClean{n});
end
Tmp = [fieldnames(DrawResults),struct2cell(DrawResults)];
DrawArgs = reshape(Tmp',[],1)';

if isempty(E.SubSumNetworkObj)
  E.createSubSumNetwork;
end
if p.Results.ForTalk
  Width = 20;
  Aspect = 2;
else
  Width = 15;
  Aspect = 0.66;
end
if ~any(strcmp(p.UsingDefaults, 'Aspect'))
  Aspect=p.Results.Aspect;
end
if iscell(p.Results.Experiment)
  Experiment = p.Results.Experiment;
else
  Experiment = {p.Results.Experiment};
end
%plot the first image of the stack
[Results, LowerResults]=E.simulateSubSum('plot',1,'YScale',1,'Save', p.Results.Save, Passthrough{:});
Res = {Results, LowerResults};
for k = 1:length(Res)
  Results = Res{k};
  if ~isempty(Results)
    if k == 1
      SSPN = 'SubSumNetworkObj';
    else
      SSPN = 'LowerSubSumNetworkHalf';
    end
    HasExperiment = ~isempty(Experiment{1}) && ~isempty(Experiment{k});
    Fig=createBasicFigure('Width',Width,'Aspect',Aspect);
    NumRes=length(Results);
    if HasExperiment
      NumPlots = NumRes + 1;
    else
      NumPlots = NumRes;
    end
    if p.Results.ForTalk
      AnnotationPos = [0.14 0.2 0.33 0.6];
      NetworkPlotSize = 2;
      PlotCols = 2;
      PlotRows = ceil((NumPlots+NetworkPlotSize)/PlotCols);
    else
      AnnotationPos = [0.14 0.2 0.33 0.6];
      NetworkPlotSize = ceil(1/Aspect);
      PlotCols = 1;
      PlotRows = NumPlots + NetworkPlotSize;
    end
    NetworkPlotPos = 1:PlotCols:NetworkPlotSize * PlotCols;
    Axes(NumPlots+1) = subplot(PlotRows,PlotCols,NetworkPlotPos,'Parent',Fig); %#ok<AGROW> %axes for drawNetwork
    %draw the network
    if k==1
      LowerHalf = false;
    else
      LowerHalf = true;
    end
    E.drawExcovNetwork('Axes', Axes(NumPlots+1), 'Figure', Fig, 'LowerHalf', LowerHalf, 'Save', false, ...
      'AnnotationPos', AnnotationPos, 'ArrowScale', 0.3, ...
      DrawArgs{:});
    if ~p.Results.ForTalk
      th = title(Axes(NumPlots+1), strrep(strrep(E.AnnotationText(1),'{','\{'),'}','\}'), 'Interpreter', 'latex');
      TitlePos = get(th, 'Position');
      TitlePos(2) = TitlePos(2) + 0.5;
      set(th, 'Position', TitlePos);
    end
    AxesPositions = setdiff(1:NetworkPlotSize * PlotCols, NetworkPlotPos);
    if NumPlots > (NetworkPlotSize * PlotCols - NetworkPlotSize)
      AxesPositions = [AxesPositions (NetworkPlotSize * PlotCols + 1):(NumPlots + NetworkPlotSize)]; %#ok<AGROW>
    end
    YLimits = zeros(NumPlots,2);
    for n=1:NumRes
      %axes for the simulation results
      Axes(n)=subplot(PlotRows,PlotCols,AxesPositions(n),'Parent',Fig);
      copyobj(allchild(Results(n).ax),Axes(n));
      YLimits(n, :) = get(Results(n).ax, 'YLim');
      set(Axes(n),'Box','off',...
        'XLim', get(Results(n).ax, 'XLim'), ...
        'YLim', YLimits(n, :), ...
        'XTick', get(Results(n).ax, 'XTick'), ...
        'XTickLabel', get(Results(n).ax, 'XTickLabel'));
      ylabel('# of agents');
      if HasExperiment
        title(Axes(n), 'Simulation');
      else
        title(Axes(n), sprintf('%d %s Agents',Results(n).Agents,Results(n).FilamentType));
      end
      if k==1 && ~E.SplitSubSumNetwork
        E.SubSumNetworkObj.annotateTargetSum(Fig, Axes(n), ...
          'YScale', 0.15, 'DrawNumber', false);
      end
      close(Results(n).fig);
    end
    if HasExperiment
      ExpRes = zeros(1, size(Results(n).Results, 2));
      ExpRes(length(ExpRes) - length(Experiment{k}) + 1:end) = Experiment{k};
      Axes(NumPlots) = subplot(PlotRows, PlotCols, AxesPositions(NumPlots), 'Parent', Fig); %#ok<AGROW>
      E.(SSPN).Results = ExpRes;
      E.(SSPN).makeplot('Save', false, 'Axes', Axes(NumPlots), ...
        'Xlimits', get(Axes(NumPlots-1), 'XLim'), ...
        'TargetArrow', false);
      YLimits(end, :) = get(Axes(NumPlots), 'YLim');
      set(Axes(NumPlots), 'FontSize', 10, 'LineWidth', 0.5);
      title(Axes(NumPlots), 'Experiment');
      legend(Axes(NumPlots),'off');
      if k==1
        E.(SSPN).annotateTargetSum(Fig, Axes(NumPlots), ...
          'YScale', 0.15, 'DrawNumber', false);
      end
      for n=1:NumPlots
        ylim(Axes(n), [min(YLimits(:, 1)), max(YLimits(:, 2))]);
      end
    end
    
    FieldsToClean={'AnnotationText', 'Save', 'Experiment'};
    AgentName=E.(SSPN).makeAgentName(Results(1).AgentName,p.Results,unique([p.UsingDefaults, FieldsToClean]));
    AgentName=regexprep(AgentName,'_YScale-1','');
    if HasExperiment
      AgentName = [AgentName, '_Experiment']; %#ok<AGROW>
    end
    if p.Results.Save
      Fig.Renderer = 'Painters';
      saveas(Fig,fullfile('results', [ellipsize( [inputname(1) '_mCP(' AgentName ')'], 241) '.pdf']));
      save(fullfile('results', [ellipsize( [inputname(1) '_mCP(' AgentName ')'], 241) '.mat']), 'Results')
      close(Fig);
    else
      set(Fig,'Visible','on');
    end
  end
end
if ~p.Results.drawResetFalse
  E.makeResetFalseJunctionMatrix;
  E.createSubSumNetwork;
end
end
