function [solution,X]=findCoveringSets(X,Y,solution)
%ExcovClass.findCoveringSets fast EXCOV solver
%implementation of the python algorithm found here:
%http://www.cs.mcgill.ca/~aassaf9/python/algorithm_x.html
%
% only finds the correct solutions, cannot find false positives.
%
% See also: solveFast, solveBruteForce, findCoveringSets

  function l=nanLength(var)
    if any(isnan(var))
      l=NaN;
    else
      l=length(var);
    end
  end

if all(cellfun(@(x) any(isnan(x)),X))
  solution={solution};
  return;
else
  %find the least frequently used element
  l=cellfun(@nanLength,X);
  [freq,set]=nanmin(l);
  %start with the sets containing the least frequenly used element and try
  %to combine them with the other sets in order to find a solution
  for n=1:freq
    solution(end+1)=X{set}(n); %#ok<AGROW>
    r=X{set}(n);
    [cols,X]=selectCols(X, Y, r);
    [solution,~]=ExcovClass.findCoveringSets(X,Y,solution);
    if iscell(solution)
      return;
    end
    %     for s in solve(X, Y, solution)
    %                 yield s
    %                 end
    [~,X]=deselectCols(X, Y, r, cols);
    solution(end)=[];
  end
end
end

function [cols,X]=selectCols(X, Y, r)
%selectCols helper function for findCoveringSets
%
% See also: findCoveringSets

cols={};
for j=Y{r}'
  for i=X{j}
    for k=Y{i}'
      if k~=j
        X{k}(X{k}==i)=[];
      end
    end
  end
  cols{end+1}=X{j}; %#ok<AGROW>
  X{j}=NaN;
end
%X=X(~cellfun('isempty',X));
end

function [cols,X]=deselectCols(X, Y, r, cols)
%selectCols helper function for findCoveringSets
%
% See also: findCoveringSets

for j=flipud(Y{r})'
  X{j} = cols{end};
  cols(end)=[];
  for i=X{j}
    for k=Y{i}'
      if k ~= j
        X{k}(end+1)=i;
      end
    end
  end
end
end
