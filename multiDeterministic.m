function Times = multiDeterministic(varargin)
p=inputParser;
p.addParameter('Numbers',primes(350),@isnumeric);
p.KeepUnmatched=true;
p.parse(varargin{:});
Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
SimArgs = reshape(Tmp',[],1)';
NNumbers=length(p.Results.Numbers);
Times = zeros(NNumbers - 1,1);
SSP = SubSumNetworkClass(p.Results.Numbers(1:2));
for n=2:NNumbers
  SSP.Numbers=p.Results.Numbers(1:n);
  tic
  SSP.simulate('Deterministic',true);
  Times(n-1) = toc;
  n
end
  
