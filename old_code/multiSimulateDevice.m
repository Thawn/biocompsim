function multiSimulateDevice(numbers)
if nargin==0
  numbers=randi(1000,1,40);
end
times=[];
figure1=figure;
axes1=axes('Parent', figure1);
for n=2:length(numbers)
  [~,times(n-1)]=simulate(numbers(1:n),'elements',1000000,'errorOP',0,'ratioORA1',0.5);%#ok<AGROW> 
  plot(axes1,2:n,times);
  xlabel('# numbers','FontSize',14);
  ylabel('computing time (s)','FontSize',14);
  save('times.mat','times');
  drawnow;
end