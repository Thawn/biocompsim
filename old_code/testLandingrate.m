function ttests=testLandingrate(rates,MT_res)
ttests=zeros(2,length(rates));
for i=1:length(rates)
  sim_res=multisim(100,[2 5 9],119,0.003,0.49,0.49,0,false,[],false,2.5,0.25,1,'',[],rates(i));
  [h,p]=ttest2(sim_res,MT_res);
  ttests(1,i)=sum(h);
  ttests(2,i)=sum(p);
end