function args=cleanUpFields(args, fields)
  found=isfield(args, fields);
  if any(found)
    fields=fields(found);
    args=rmfield(args, fields);
  end