function agents=agentNumberNeeded(numbers,errorOP,a,b)
if nargin==1
  errorOP=0;
end
if nargin<4
  a=19;
  b=0.705;
end
agents=round(a*exp(b*length(numbers)));
if errorOP>0
  percentage=percentCorrect(numbers,errorOP);
  agents=agents/percentage;
end
