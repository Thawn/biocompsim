function [agents time minimum]=estimateAgentNumberNeeded(numbers,varargin)
%errorOP,ratioORA1,ratioORB2,a,b,plot,logErrors,normalize,s,h,x)
p=inputParser;
%define the parameters
p.addRequired('numbers',@isnumeric);
p.addOptional('a',19,@isnumeric);
p.addOptional('b',0.705,@isnumeric);
p.addParamValue('elements',1000,@isnumeric); %this is just to catch that variable if entered by the user. We calculate the number of agents ourselves

p.KeepUnmatched=true;

p.parse(numbers,varargin{:});

if isfield(p.Results,'elements')
  warning('biocomp_sim:estimateAgentNumberNeeded','parameter "elements" was given but will be ignored. The number of elements is calculated within the function from the parameters "a" and "b".');
end

a=p.Results.a;
b=p.Results.b;

tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
simArgs = reshape(tmp',[],1)';

agents=round(a*exp(b*length(numbers))) %#ok<*NOPRT>

valid=determineCorrectSums(numbers);
[results,time]=simulate(numbers,'elements',agents,simArgs{:});

minimum=min(results(valid))
while minimum<10||minimum>20
  if minimum>0
    agents = round(agents*15/minimum)
  else
    agents = round(agents*15)
  end
  [results,time]=simulate(numbers,'elements',agents,simArgs{:});
  minimum=min(results(valid))
end