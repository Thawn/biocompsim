function [results,valid,invalid,errors]=plotSim(numbers,varargin)
%elements,errorOP,ratioORA1,ratioORB2,plot,logErrors,normalize,s,h,x,hybridComputerNumbers,hybridBioNumbers,AnnotationText,Xlimits,landingRate,Ylimits)
p=inputParser;
%define the parameters
p.addRequired('numbers',@isnumeric);
%p.addOptional('elements',10000,@isnumeric);
%p.addOptional('errorOP',0.01,@isnumeric);
%p.addOptional('ratioORA1',0.5,@isnumeric);
%p.addOptional('ratioORB2',[],@isnumeric);
p.addParameter('plot',2,@isnumeric);
p.addParameter('logErrors',false,@islogical);
% p.addParamValue('normalize',false,@islogical);
% p.addParamValue('s',1.22,@isnumeric);
% p.addParamValue('h',0.57,@isnumeric);
% p.addParamValue('x',1,@isnumeric);
p.addParameter('AnnotationText','',@(x)ischar(x) || iscellstr(x) );
%p.addParamValue('hybridComputerNumbers',[],@isnumeric);
%p.addParamValue('hybridBioNumbers',[],@isnumeric);
%p.addParameter('FastHybrid',false,@islogical);
p.addParameter('DirectInput',[],@isnumeric);
p.addParameter('Xlimits',[],@isnumeric);
p.addParameter('Ylimits',[],@isnumeric);
%p.addParamValue('landingRate',0,@isnumeric);

p.KeepUnmatched=true;

p.parse(numbers,varargin{:});

%elements=p.Results.elements;
%errorOP=p.Results.errorOP;
%ratioORA1=p.Results.ratioORA1;
%if isempty(p.Results.ratioORB2)
%  ratioORB2=ratioORA1;
%else
%  ratioORB2=p.Results.ratioORB2;
%end
logErrors=p.Results.logErrors;
% normalize=p.Results.normalize;
% s=p.Results.s;
% h=p.Results.h;
% x=p.Results.x;
AnnotationText=p.Results.AnnotationText;
%hybridComputerNumbers=p.Results.hybridComputerNumbers;
%hybridBioNumbers=p.Results.hybridBioNumbers;
Xlimits=p.Results.Xlimits;
Ylimits=p.Results.Ylimits;
%landingRate=p.Results.landingRate;


tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
simArgs = reshape(tmp',[],1)';

[valid, invalid]=determineCorrectSums(numbers);
if ~isempty(p.Results.DirectInput)
  valid=fastHybridCalculateResults(valid,p.Results.DirectInput)>0;
  invalid=~valid;
end
[results,~,errors]=simulate(numbers,simArgs{:},'progress',p.Results.plot>0,'logErrors',logErrors,'DirectInput',p.Results.DirectInput);
if p.Results.plot>0
  makeplotArgs={'save',p.Results.plot>1,'AnnotationText',AnnotationText,'Xlimits',Xlimits,'Ylimits',Ylimits};
  disp(['min:' num2str(min(results(valid)))]);
  temp=struct('numbers',numbers); %#ok<NASGU>
  agentName=evalc('disp(temp)');
  fieldsToClean={'AnnotationText','numbers','plot'};
  agentName=makeAgentName(agentName,p.Unmatched, fieldsToClean);
  agentName=makeAgentName(agentName,p.Results,unique([p.UsingDefaults, fieldsToClean]));
  if isempty(agentName)
    agentName='allDefaults';
  end
  if logErrors
    makeplot(results, errors, 'agentName', agentName, 'correctErr',sqrt(results), 'wrongErr',sqrt(errors), 'stacked',true, makeplotArgs{:});
  else
    correct=results.*valid;
    incorrect=results.*invalid;
    makeplot(correct, incorrect, 'agentName', agentName, 'correctErr',sqrt(correct), 'wrongErr',sqrt(incorrect), 'stacked',false, makeplotArgs{:});
  end
end

