function [h,prob]=estimateCalculationSignificance(numbers,results,varargin)
%estimateCalculationSignificance this function calculates whether the results of the calculation are significant compared to the noise.

p=inputParser;
p.addRequired('numbers',@isnumeric);
p.addRequired('results',@isnumeric);
p.addParamValue('alpha',0.05,@isnumeric);
p.addParamValue('eliminate',[],@isnumeric);
p.parse(numbers,results,varargin{:});

[valid, invalid]=determineCorrectSums(p.Results.numbers);
results=p.Results.results;
if ~isempty(p.Results.eliminate)
  valid(p.Results.eliminate)=[];
  invalid(p.Results.eliminate)=[];
  results(p.Results.eliminate)=[];
end

noise=results(invalid)';
correct=results(valid);
ps=zeros(1,length(correct));
for i=1:length(correct)
  [~,ps(i)]=ttest2(noise,correct(i));
end
ps=1-ps;
prob=prod(ps);
prob=1-prob;
h=prob<p.Results.alpha;