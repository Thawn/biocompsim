function multiSimulateDeterministicDevice(numbers)
if nargin==0
  numbers=randi(1000,1,40);
end
times=[];
figure1=figure;
axes1=axes('Parent', figure1);
for n=2:length(numbers)
  tic
  deterministicSimulate(numbers(1:n));
  %subSumTheHardWay(numbers(1:n));
  times(n-1)=toc; %#ok<AGROW>
  plot(axes1,2:n,times);
  xlabel('# numbers','FontSize',14);
  ylabel('computing time (s)','FontSize',14);
  save('times.mat','times');
  drawnow;
end