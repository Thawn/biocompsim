function [results,valid,invalid,errors]=plotSim(numbers,elements,errorOP,ratioORA1,ratioORB2,plot,logErrors,normalize,s,h,x)
if nargin<3
  errorOP=0.05;
end
if nargin<4
  ratioORA1=0.5;
end
if nargin<5
  ratioORB2=ratioORA1;
end
if nargin<6
  plot=2;
end
if nargin<7
  logErrors=false;
end
if nargin<8
  normalize=false;
end
if nargin<9
  s=2.5;
end
if nargin<10
  h=0.25;
end
if nargin<11
  x=1;
end
errors=[];
[valid, invalid]=determineCorrectSums(numbers);
if logErrors
  [results,~,errors]=simulate(numbers,elements,errorOP,ratioORA1,ratioORB2,plot,true,normalize,s,h,x);
else
  results=simulate(numbers,elements,errorOP,ratioORA1,ratioORB2,plot,false,normalize,s,h,x);
end
if plot>0
  disp(['min:' num2str(min(results(valid)))]);
  if normalize
    normalized=[' corrected: ' num2str(s) '; ' num2str(h) '; ' num2str(x)];
  else
    normalized='';
  end
  %figure();
  numResults=length(results);
  X=0:numResults-1;
  XTicks=X;
  if numResults>20
    step=ceil(numResults/50);
    step=step*5;
    XTicks=XTicks(1:step:end);
  end
  if logErrors
    h=bar(X,[errors' results'],'stacked');
    plot1=h(2);
    plot2=h(1);
    ylim=[0 max(results+errors)*1.2];
    stacked=' errors logged';
    edgeColor='none';
  else
    correct=results.*valid;
    incorrect=results.*invalid;
    plot1=bar(X,correct,'g');
    hold on;
    plot2=bar(X,incorrect,'m');
    ylim=[0 max(results)*1.2];
    stacked='';
    edgeColor=[0 0 0];
  end
  set(gca, 'XTick', XTicks, 'XLim', [-1 length(results)], 'YLim',ylim, 'Box', 'off', 'PlotBoxAspectRatioMode', 'manual', 'PlotBoxAspectRatio', [16 9 1],'FontSize', 12,'LineWidth', 0.75);
  set(plot1,'DisplayName','correct','FaceColor','g','EdgeColor', edgeColor);
  set(plot2,'DisplayName','incorrect','FaceColor','m','EdgeColor', edgeColor);
  legend1=legend([plot1 plot2]);
  set(legend1,'Orientation','horizontal','Location','Best','FontSize',12);
  ylabel('# of agents','FontSize',14);
  xlabel('exit #','FontSize',14);
  hold off;
  if plot>1
    set(gcf,'Units','Inches');
    pos = get(gcf,'Position');
    set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
    saveas(gcf,['results' filesep 'plotSim(numbers ' num2str(numbers) ' agents ' num2str(elements) ' errorOP ' num2str(errorOP) ' ratioORA1 ' num2str(ratioORA1) ' ratioORB2 ' num2str(ratioORB2) stacked normalized ').pdf']);
  end
end
end
