function [agents,times,minimum]=multiEstimateAgentNumber(numbers,varargin)
%errorOP,ratioORA1,ratioORB2,a,b,logErrors,normalize,s,h,x)

p=inputParser;
%define the parameters
p.addRequired('numbers',@isnumeric);
p.addOptional('a',19,@isnumeric);
p.addOptional('b',0.705,@isnumeric);
p.addParamValue('elements',1000,@isnumeric); %this is just to catch that variable if entered by the user. We calculate the number of agents ourselves

p.KeepUnmatched=true;

p.parse(numbers,varargin{:});

if isfield(p.Results,'elements')
  warning('biocomp_sim:estimateAgentNumberNeeded','parameter "elements" was given but will be ignored. The number of elements is calculated within the function from the parameters "a" and "b".');
end

a=p.Results.a;
b=p.Results.b;

tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
simArgs = reshape(tmp',[],1)';

times=[];
agents=[];
minimum=[];
figure1=figure;
axes1=axes('Parent', figure1);
for n=2:length(numbers)
  [agents(n-1), times(n-1), minimum(n-1)]=estimateAgentNumberNeeded(numbers(1:n),simArgs{:});%#ok<AGROW>
  plot(axes1,2:n,agents,'r',2:n,times,'b');
  xlabel('# numbers','FontSize',14);
  ylabel('computing time (blue) number of agents required (red)','FontSize',14);
  drawnow;
  save('times.mat','times','agents','minimum');
end