function [Sets, SubSumNumbers]=deOptimizeExcovSet(Sets,Mapping)
Sets=[Mapping Sets]; %add the original map to the sets
Sets=sortrows(Sets); %sort the sets according to the original mapping to the lowest bits
Mapping=Sets(:,1); %extract the new sorted mapping of the bits to the numbers
Sets=Sets(:,2:end); %extract the remapped sets
SubSumNumbers=bin2dec(num2str(Sets'))';