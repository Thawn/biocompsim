function plotAgents(X1, Y1, X2, YMatrix1)
%CREATEFIGURE(X1,Y1,X2,YMATRIX1)
%  X1:  vector of x data
%  Y1:  vector of y data
%  X2:  vector of x data
%  YMATRIX1:  matrix of y data

%  Auto-generated by MATLAB on 17-Dec-2012 13:38:37

% Create figure
figure1 = figure('XVisual','');

% Create axes
axes1 = axes('Parent',figure1,'TickDir','out');
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[2 20]);
hold(axes1,'all');

% Create plot
plot(X1,Y1,'Parent',axes1,'Marker','o','DisplayName','0% error rate');

% Create multiple lines using matrix input to plot
plot1 = plot(X2,YMatrix1,'Parent',axes1);
set(plot1(1),'Marker','square','DisplayName','0.5 % error rate');
set(plot1(2),'Marker','x','DisplayName','1% error rate');

% Create xlabel
xlabel('cardinality (number of primes)','FontSize',12);

% Create ylabel
ylabel('# of agents','FontSize',12);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
  'Position',[0.15639880952381 0.799042807597441 0.209077380952381 0.114451476793249]);

