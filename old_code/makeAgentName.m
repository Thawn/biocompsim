function agentName=makeAgentName(agentName,nameValueStruct,fieldsToClean)
temp=cleanUpFields(nameValueStruct, fieldsToClean); 
if ~isempty(agentName)
  newline=sprintf('\n');
  agentName=[agentName newline];
end
if ~isempty(fields(temp))
  agentName=[agentName evalc('disp(temp)')];
end
agentName=regexprep(agentName,'\n+','_');
agentName=regexprep(agentName,'(\.\d*)0+_','$1_');
agentName=regexprep(agentName,'(\.\d*?)0+ ','$1 ');
agentName=regexprep(agentName,'(\.\d*?)0+]','$1]');
agentName=regexprep(agentName,'\s{2,}','');
agentName=strrep(agentName,': ','-');
agentName=regexprep(agentName,'\_+$','');
