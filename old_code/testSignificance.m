function sig=testSignificance(runs,sim_res)
sig=zeros(1,runs);
for i=1:runs
  single_res=simulate([2 5 9],'elements',119,'errorOP',0.003,'ratioORA1',0.49,'landingRate',0.15);
  h=ttest2(sim_res,single_res);
  sig(i)=sum(h);
end