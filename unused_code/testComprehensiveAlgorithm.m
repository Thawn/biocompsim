function E=testComprehensiveAlgorithm(varargin)
p=inputParser;
%define the parameters
p.addParameter('HasErrors','no',@ischar);
p.addParameter('HasCover','yes',@ischar);
p.KeepUnmatched=true;
p.parse(varargin{:});
tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
excovArgs = reshape(tmp',[],1)';
if strcmp(p.Results.HasCover,'no')&&~isfield(p.Unmatched,'Satisfiable')
  excovArgs{end+1}='Satisfiable';
  excovArgs{end+1}=false;
end
done=false;
while ~done
  E=ExcovClass(excovArgs{:});
  E.ComprehensiveAnalyzeExactCover;
  done=~strcmp(E.hasCover,p.Results.HasCover) || ( strcmp(E.hasCover,'no')&& strcmp(E.hasError,'yes'));
end
disp(E);