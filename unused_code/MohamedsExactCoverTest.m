function [testResult, SSintegers, C, isExCov, errors] = ExactCoverTest(S)

% Input arguments
%
% S:    Set to be tested
%
% Output arguments
%
% SSintegers:   Integer values in the subset sum problem corresponding to S
%
% C:    Cell array whose ith cell contains all size-i subsets of the set S.
%       Each subset in the cell is represented by a row containing the
%       indeces of the subset's elements in the set S.
%
% isExCov:  Cell array whose ith cell contains a column vector whose jth
%           element is 1 if the corresponding jth subset in the ith cell of
%           C is an exact cover of S.
%
% error:    Cell array whose ith cell contains a column vector whose jth
%           element is 1 if binary addition misclassified the corresponding
%           jth subset in the ith cell of C. Misclassification occurs when
%           either
%
%           1) the binary sum of an excat cover subset is not a sequence of
%           ones, or
%           2) the binary sum of a subset that is not an exact cover is a
%           sequence of ones

setSize = length(S);

% Find the maximum number in the set
maxNum = 0;
for i = 1:setSize
  if max(S{i}) > maxNum
    maxNum = max(S{i});
  end
end

% Construct the alphabet with only those numberss that appear in at least
% one element of the set S
alphabet = [];
for i = 1:maxNum
  for j = 1:setSize
    if ~isempty(find(S{j}==i))
      alphabet = [alphabet i];
      break;
    end
  end
end

% Size of the alphabet
alphabetSize = length(alphabet);

% Find all possible subsets
v = 1:setSize;
C = {};
for i = 1:setSize
  C{i} = nchoosek(v,i);   % Each row in C{i} contains the indeces of the i elements of a subset
end

% Translate each element in S to its binary code
binM = zeros(setSize,alphabetSize);
SSintegers = zeros(setSize,1);      % corresponding subset sum integers
for i = 1:setSize
  for j = 1:alphabetSize
    if ~isempty(find(S{i} == alphabet(j)))
      binM(i,alphabetSize-j+1) = 1;
    end
  end
  SSintegers(i) = bin2dec(num2str(binM(i,:),'%1d'));
end

errorExists = 0;
hasExCov = 0;
isExCov = {};
errors = {};
for i = 1:setSize
  M = C{i};
  isExCov{i} = zeros(size(M,1),1);
  errors{i} = zeros(size(M,1),1);
  subsetSize = size(M,2);
  for j = 1:size(M,1)
    % Construct a subset and it's binary code
    subset = {};
    binCode = zeros(subsetSize,alphabetSize);   % subset's binary matrix
    for k = 1:subsetSize
      subset{k} = S{M(j,k)};
      binCode(k,:) = binM(M(j,k),:);
    end
    
    % Subset is an exact cover if each column has only one 1 in it
    subsetsum = sum(binCode,1);
    if sum(subsetsum)==alphabetSize && isempty(find(subsetsum == 0))
      hasExCov = 1;
      isExCov{i}(j) = 1;
    end
    
    % Convert the rows of the binary matrix to decimal and add them up
    decSum = 0;
    for k = 1:size(binCode,1)
      decSum = decSum+bin2dec(num2str(binCode(k,:),'%1d'));
    end
    
    % Convert the sum back to binary
    binSum = dec2bin(decSum);
    
    % Make sure the length of the binary sum is equal to alphabet size
    if length(binSum)<alphabetSize
      binSum = [num2str(zeros(1,alphabetSize-length(binSum)),'%1d') binSum];
    else
      binSum = fliplr(binSum);
      binSum = binSum(1:alphabetSize);
      binSum = fliplr(binSum);
    end
    
    % Binary addition scewed up if
    % 1) the binary sum of an excat cover subset is not a sequence of ones
    % OR
    % 2) the binary sum of a subset that is not an exact cover is a sequence of ones
    if isExCov{i}(j) ~= strcmp(binSum,num2str(ones(1,alphabetSize),'%1d'))
      errors{i}(j) = 1;
      errorExists = 1;
    end
  end
end

testResult=cell(4,1);
testResult{1}='Exact covers:';
if hasExCov
  testResult{2}=printSet(setSize,isExCov,S,C);
else
  testResult{2}='none';
end

testResult{3}='Subsets misclassified by binary addition:';
if errorExists
  testResult{4}=printSet(setSize,errors,S,C);
else
  testResult{4}='none';
end
end
function resultText=printSet(setSize,resultSets,S,C)
resultText='';
for i = 1:setSize
  subsetSize = i;
  for j = 1:length(resultSets{i})
    if resultSets{i}(j)
      resultText=[resultText,'{'];
      for k = 1:subsetSize
        resultText=[resultText,'(', num2str(S{C{i}(j,k)}), ')'];
        if k < subsetSize
          resultText=[resultText,','];
        end
      end
      resultText=[resultText,'}'];
    end
  end
end
end


