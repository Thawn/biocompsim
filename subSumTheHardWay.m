function results = subSumTheHardWay(numbers)
%subSumTheHardWay solves the subset sum problem defined by numbers by brute
%force.

results=[];
for n=1:length(numbers)
  subset=combnk(numbers, n);
  results=[results; sum(subset,2)]; %#ok<AGROW>
end
results=unique(results);
