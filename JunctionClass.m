classdef JunctionClass < handle
  %JunctionClass class used to simulate the behavior of agents at
  %individual junctions within a subset sum network.
  %
  % See also: SubSumNetworkClass,DeterministicJunctionClass,LogErrorJunctionClass,CorrectedJunctionClass,CorrectedLogErrorJunctionClass
  
  properties
    ErrorOP = 0.003; %error rate at pass junctions left entrance.
    ErrorOPB = 0.003; %error rate at pass junctions top entrance.
    RatioORA1 = 0.5; %split ratio at split junction entering at the left entrance
    RatioORB2 = 0.5; %split ratio at split junction entering at the top entrance
    DiscardReset = false; %whether ResetFalse junctions should discard agents moving to the left instead of forcing them downward. Improves error predictability.
  end
  methods
    
    
    function J = JunctionClass(varargin)
      if nargin>0
        p=inputParser;
        p.addParameter('Optimize',true,@islogical);
        p.KeepUnmatched=true;
        
        p.parse(varargin{:});
        
        Names=fieldnames(p.Unmatched);
        for i=1:length(Names)
          if isempty(J.findprop(Names{i}))
            warning(strcat('JunctionClass does not support the parameter: ', Names{i}));
          else
            J.(Names{i})=p.Unmatched.(Names{i});
          end
        end
        if ~ismember('RatioORB2', Names) && ismember('RatioORA1', Names)
          J.RatioORB2 = J.RatioORA1;
        end
        if ~ismember('ErrorOPB', Names) && ismember('ErrorOP', Names)
          J.ErrorOPB = J.ErrorOP;
        end
      end
    end
    
    
    function [Output1, Output2] = calculate(J, InputB, InputA, JunctionType) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate calculates the behavior of agents in a junction. 
      %
      % Parameters:
      %   JunctionType: string, name of the junction, part of the function
      %   that will be called for the actual calculation.
      %   InputB: number of agents entering from the top.
      %   InputA: number of agents entering from the left.
      %
      % See also: calculateLineSegment, monteCarlo
      
      if nargin < 4
        JunctionType = 'Pass';
      end
      switch JunctionType
        case 'Split'
          [Output1, Output2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
        case 'SplitTop'
          [Output1, Output2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.ErrorOP);
        case 'SplitLeft'
          [Output1, Output2] = J.monteCarlo(InputB, InputA, J.ErrorOPB, J.RatioORA1);
        case 'ResetFalse'
          if J.DiscardReset
            [Output1, ~] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
          else
            Output1 = InputB + InputA;
          end
          Output2 = 0;
        case 'ResetTrue'
          if J.DiscardReset
            [~, Output2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
          else
            Output2 = InputB + InputA;
          end
          Output1 = 0;
        otherwise % Pass junction is the default
          [Output1, Output2] = J.monteCarlo(InputB, InputA, J.ErrorOPB, J.ErrorOP);
      end
    end
    
    
    function Register=calculateLineSegment(J, Register, ind, JunctionType)
      %calculateLineSegment calculates the behavior of agents for the 
      %current part of a line of a subset sum network.
      %
      % See also: SubSumNetworkClass.simulate
      
      n = ind * 2 - 1;
      [one, two] = J.calculate(Register(1, n), Register(1, n + 1), JunctionType);
      Register(2, n) = Register(2, n) + one;
      Register(2, n + 3) = Register(2, n + 3) + two;
    end
    
    
  end
  methods (Static)
    
    
    function [Output1, Output2] = monteCarlo(InputB, InputA, TurningRateB, TurningRateA) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %monteCarlo randomly distributes agents from inputA and inputB to
      %output1 and output2 depending on  and TurningRateA. 
      %On average, the ratio with which the agents are distributed is
      %determined by the TurningRateA for inputA and TurningRateB for inputB.
      %
      % See also: calculate
      
      A=rand(InputA,1);
      B=rand(InputB,1);
      A(A<TurningRateA)=[];
      B(B<TurningRateB)=[];
      Output1=length(B)+(InputA-length(A));
      Output2=length(A)+(InputB-length(B));
    end
    
    
    function Register = createLineRegister(ProblemSize)
      %createLineRegister create a register of the appropriate dimension
      %and type for this junction class.
      %
      %  We make the register two elements larger than needed to cope with
      %  filaments that leave the device to the right. (only happens with randomly
      %  landing fiaments or if we set a target sum smaller than the maximum sum).
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register = zeros(2, (ProblemSize + 1) * 2, 'uint32');
    end
    
    
    function Register = shiftLineRegister(Register)
      %shiftLineRegister after processing a line is complite, this function
      %shifts the lines of a register sucht that processing of the next
      %line can begin.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register(1, :) = Register(2, :);
      Register(2, :) = 0;
    end
    
    
    function Results = calculateResults(Register, ProblemSize)
      %calculateResults calculates the actual results from the register used for simulation.
      %
      % See also: simulate
      
      Results = zeros(2, ProblemSize);
      Results(1, :) = Register(1, 1:2:ProblemSize * 2) + Register(1, 2:2:ProblemSize * 2);
    end
    
    
  end
end