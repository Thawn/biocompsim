#Simulation tool for subset-sum-type biocomputation networks

This tool is used for stochastic simulations of subset-sum-type biocomputation 
networks.
The two main classes are SubSumNetworkClass which simulates the subset 
sum problem (SSP) network and the ExcovClass which handles the conversion of excov problems
into SSP networks.

In the following, the main classes that perform the simulations are described.

## SubSumNetworkClass
Simulate Subset Sum networks. 

### Examples:

* Create a new subset sum problem instance for the problem {5 11 17 29 31} :
```matlab
SSP=SubSumNetworkClass(primes(29))
````
* Get a visual representation of all legal paths through the network:
```matlab
Figure=SSP.drawNetwork
Figure.Visible='on'
```
* Run 10 simulations and plot the mean of the results
```matlab
Figure=SSP.multisim('Iterations', 10)
Figure.Visible='on'
```

## ExCovClass

Simulate Exact Cover networks.

### Examples:
* Create a random Exact Cover (EXCOV) instance. The integers in the sets are between 1 and 8 and we generate 8 sets:
```matlab
EXCOV=ExcovClass('numInts',8,'numSets',8)
```
* Alternatively, you can create your own excov problem. To define the sets, you create a matrix with I rows and N columns, where I is the maximum integer and N is the number of sets:
```matlab
Sets=zeros(8,8)
```
* For each set, you set the row that corresponds to your integers in that set to one. In this example, we define the first set as {1; 4; 7; 8}:
```matlab
Sets(:,1)=[1;0;0;1;0;0;1;1]
Sets(:,2)=[0;0;0;0;1;0;0;1]
```
* Now do the same for all remaining sets.
* After you have defined the sets, you can create the ExcovClass:
```matlab
EXCOV=ExcovClass('inputSets',Sets)
```
* By default, the mapping of the integers will be optimized such that the resulting SSP numbers are as small as possible. If you want to avoid this, you can turn off optimization:
```matlab
EXCOV=ExcovClass('inputSets',Sets,'Optimize',false)
```
* Get a visual representation of all legal paths through the resulting optimized SSP network:
```matlab
Figure=EXCOV.drawExcovNetwork
Figure.Visible='on'
```
* Simulate and plot 10 simulations with default parameters for actin and microtubules and plot them:
```matlab
Figure=EXCOV.makeCombinedPlot
Figure.Visible='on'
```

## SATClass
simulates solving SAT problems with tagging in network-based biocomputation

## SpaceSATClass
simulates solving SAT problems without tagging in network-based biocomputation
