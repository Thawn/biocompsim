classdef DeterministicJunctionClass < JunctionClass
  %DeterministicJunctionClass class used to quickly find the correct exits
  %of a subset sum network.
  %
  % See also: SubSumNetworkClass,JunctionClass,LogErrorJunctionClass,CorrectedJunctionClass,CorrectedLogErrorJunctionClass
  
  properties
  end
  methods
    
    
    function J = DeterministicJunctionClass(varargin)
      J@JunctionClass(varargin{:})
    end
    
    
    function Register=calculateLineSegment(J,Register, ind, JunctionType)
      %calculateLineSegment calculates the behavior of agents for the 
      %current part of a line of a subset sum network.
      %
      % See also: SubSumNetworkClass.simulate
      
      n = ind * 2 - 1;
      [one, two] = J.calculate(Register(1, n), Register(1, n + 1), JunctionType);
      Register(2, n) = one;
      Register(2, n + 3) = two;
    end
    
    
  end
  methods (Static)
    
    
    function Register=createLineRegister(ProblemSize)
      %createLineRegister create a register of the appropriate dimension
      %and type for this junction class.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register = zeros(2, (ProblemSize + 1) * 2, 'logical');
    end
    
    
    function [Output1, Output2] = calculate(InputB, InputA, JunctionType) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate calculates the behavior of agents in a junction. 
      %
      % Parameters:
      %   JunctionType: string, name of the junction, part of the function
      %   that will be called for the actual calculation.
      %   InputB: number of agents entering from the top.
      %   InputA: number of agents entering from the left.
      %
      % See also: calculateLineSegment, monteCarlo
      
      if nargin < 3
        JunctionType = 'Pass';
      end
      switch JunctionType
        case 'Split'
          Output1 = InputB | InputA;
          Output2 = Output1;
        case 'SplitTop'
          Output1 = InputB;
          Output2 = InputB | InputA;
        case 'SplitLeft'
          Output1 = InputB | InputA;
          Output2 = InputA;
        case 'ResetFalse'
          Output1 = InputB | InputA;
          Output2 = 0;
        case 'ResetTrue'
          Output1 = 0;
          Output2 = InputB | InputA;
        otherwise % Pass junction is the default
          Output1 = InputB;
          Output2 = InputA;
      end
    end
    
    
  end
end