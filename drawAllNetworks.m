% calls the drawNetwork command on all ExcovClass objects defined in the
% SetNames variable. The variable SetNames and ExcovClass objects must be
% present in the current workspace.

for n=1:length(SetNames)
  mkdir(fullfile('results',SetNames{n}));
%   ev=[SetNames{n} '.makeAnnotation'];
%   evalStr=[SetNames{n} '.makeCombinedPlot(''FilamentTypes'',{''optimized MT'',''optimized Actin''},',...
%     '''ErrorOP'',[0 0.0054],''RatioORA1'',[0.453 0.472],''RatioORB2'',[0.667 0.535]'];
  evalStr=[SetNames{n} '.makeCombinedPlot(''NumCores'',2,''LogErrors'',true'];
  evalStr1=[evalStr ');'];
  evalStr3=[evalStr ',''drawResetFalse'',false);'];
%   eval(ev);
%   ev=[SetNames{n} '.createSubSumNetwork'];
%   eval(ev);
  eval(evalStr1);
  eval(evalStr3);
  close all;
end
clear('n','ev','evalStr','evalStr1','evalStr2','evalStr3','evalStr4');