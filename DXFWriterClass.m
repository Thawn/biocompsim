classdef DXFWriterClass < handle
  %DXFWriterClass takes a base layout DXF file and uses instances of
  %junctions in that file to write a new DXF file containing a layout for a
  %certain biocomputation network.
  
  properties
      Scale = 1.5;
      BaseLayout = 'Base_layout_fabrication_corrected.DXF';
      OutputFile;
      LayoutFile;
      LineEnd = '\n'
      Font = 'Font.DXF';
      SupportedChars='1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ.';
      CharacterSpacing = 2.3;
      FontAdded = false;
      HeaderWritten = false
  end
  
  methods
    
    
    function DXFW = DXFWriterClass(OutputFileName, varargin)
      if ispc
        DXFW.LineEnd='\r\n';
      end
      if nargin > 0
        p=inputParser;
        p.addRequired('OutputFileName',@ischar);
        p.KeepUnmatched=true;
        p.parse(OutputFileName, varargin{:});
        Names=fieldnames(p.Unmatched);
        for i=1:length(Names)
          if isempty(DXFW.findprop(Names{i}))
            warning(strcat('SurfaceClass does not support the parameter: ',Names{i}));
          else
            DXFW.(Names{i})=p.Unmatched.(Names{i});
          end
        end
        DXFW.LayoutFile = fopen(DXFW.BaseLayout,'r','n','UTF-8');
        DXFW.OutputFile = fopen(p.Results.OutputFileName,'w','n','UTF-8');
      end
    end
    
    
    function delete(DXFW)
      if ~isempty(DXFW.LayoutFile)
        Filename = fopen(DXFW.LayoutFile);
        if ~isempty(Filename)
          fclose(DXFW.LayoutFile);
        end
      end
      if ~isempty(DXFW.OutputFile)      
        Filename = fopen(DXFW.OutputFile);
        if ~isempty(Filename)
          fprintf(DXFW.OutputFile,['0' DXFW.LineEnd 'ENDSEC' DXFW.LineEnd,...
            '0' DXFW.LineEnd 'EOF' DXFW.LineEnd]);
          fclose(DXFW.OutputFile);
        end
      end
    end
    
    
    function DXFW = writeDXFHeader(DXFW,varargin)
      if ~DXFW.HeaderWritten
        p=inputParser;
        p.addParameter('AddFont',true,@islogical);
        p.parse(varargin{:});
        DXFW.copyUntilKeyWord('BLOCKS');
        if p.Results.AddFont
          DXFW.addDXFFont;
        end
        DXFW.copyUntilKeyWord('ENTITIES');
        fclose(DXFW.LayoutFile);
        fprintf(DXFW.OutputFile,['ENTITIES' DXFW.LineEnd]);
        DXFW.HeaderWritten = true;
      end
    end
    
    
    function DXFW = copyUntilKeyWord(DXFW,Keyword)
        Content=fgetl(DXFW.LayoutFile);
        while ~strcmp(Content,Keyword)
          if all(Content==-1)
            error('MATLAB:biocomp_sim:DXFWriterClass:writeDXFHeader','Invalid layout file: %s No %s keyword found.',DXFW.BaseLayout,Keyword)
          end
          fprintf(DXFW.OutputFile,['%s' DXFW.LineEnd],Content);
          Content=fgetl(DXFW.LayoutFile);
        end
    end
    
    
    function makeInsert(DXFW,Name,X,Y,varargin)
      p = inputParser;
      p.addParameter('Rotation', 0, @isnumeric);
      p.addParameter('Scale', DXFW.Scale, @isnumeric);
      p.addParameter('FlipX', false, @islogical);
      p.parse(varargin{:});
      if p.Results.FlipX
        Flip = -1;
      else
        Flip = 1;
      end
      fprintf(DXFW.OutputFile,['0' DXFW.LineEnd 'INSERT' DXFW.LineEnd '8' DXFW.LineEnd '0'...
        DXFW.LineEnd '2' DXFW.LineEnd '%s' DXFW.LineEnd '10' DXFW.LineEnd '%g' DXFW.LineEnd '20'...
        DXFW.LineEnd '%g' DXFW.LineEnd '41' DXFW.LineEnd '%g' DXFW.LineEnd '42' DXFW.LineEnd '%g'...
        DXFW.LineEnd '50' DXFW.LineEnd '%g' DXFW.LineEnd], Name, X * p.Results.Scale, Y * p.Results.Scale,...
        Flip * p.Results.Scale, p.Results.Scale, p.Results.Rotation);
    end
    
    
    function makeLWPolyLine(DXFW,X,Y,Layer)
      if nargin < 4
        Layer = 'L1D0';
      end
      X=X.*DXFW.Scale;
      Y=Y.*DXFW.Scale;
      NPoints=length(X);
      fprintf(DXFW.OutputFile,['0' DXFW.LineEnd 'LWPOLYLINE' DXFW.LineEnd '8' DXFW.LineEnd...
        Layer DXFW.LineEnd '90' DXFW.LineEnd '%g' DXFW.LineEnd '70' DXFW.LineEnd '1' DXFW.LineEnd...
        '43' DXFW.LineEnd '0' DXFW.LineEnd],NPoints);
      for n=1:NPoints
        fprintf(DXFW.OutputFile,['10' DXFW.LineEnd '%g' DXFW.LineEnd '20' DXFW.LineEnd '%g' DXFW.LineEnd],X(n),Y(n));
      end
    end
    
    
    function DXFW = annotateDXF(DXFW,Text,X,Y,Rotation,TextOffset,TextScale)
      if nargin<5
        Rotation=0;
      end
      if nargin<6
        TextOffset=[0 0];
      end
      if nargin<7
        TextScale=1;
      end
      if ~DXFW.FontAdded
        DXFW.addDXFFont
      end
      if TextScale > 0
          X=(X+TextOffset(1))/TextScale;
          Y=(Y+TextOffset(2))/TextScale;
          for n=1:length(Text)
              Char=upper(Text(n));
              if ~isempty(strfind(DXFW.SupportedChars,Char))
                  if strcmp(Char,'.')
                      Char='Stop';
                  end
                  DXFW.makeInsert(Char,X,Y,'Rotation',Rotation,'Scale',DXFW.Scale*TextScale)
              end
              CharOffset = [ DXFW.CharacterSpacing 0 ];
              Angle=Rotation/180*pi; %convert angle from degrees to radians
              R=[cos(Angle),sin(Angle);-sin(Angle),cos(Angle)];
              CharOffset=CharOffset*R;
              X=X+CharOffset(1);
              Y=Y+CharOffset(2);
          end
      end
    end
   
    
    function DXFW = addDXFFont(DXFW)
      if ~DXFW.FontAdded
        FontFile=fopen(DXFW.Font,'r','n','UTF-8');
        Content=fgetl(FontFile);
        while ~strcmp(Content,'BLOCKS')
          if all(Content==-1)
            error('MATLAB:biocomp_sim:SubSumNetworkClass:getDXFFont','Invalid Font.DXF file: No BLOCKS section found.')
          end
          Content=fgetl(FontFile);
        end
        while ~strcmp(Content,sprintf('ENDSEC'))
          if all(Content==-1)
            error('MATLAB:biocomp_sim:SubSumNetworkClass:getDXFFont','Invalid Font.DXF file: No ENDSEC found for BLOCKS section.')
          end
          fprintf(DXFW.OutputFile,['%s' DXFW.LineEnd],Content);
          Content=fgetl(FontFile);
        end
        fclose(FontFile);
        DXFW.FontAdded = true;
        %skipping a line of the LayoutFile because the BLOCKS section is
        %already added from the FontFile.
        fgetl(DXFW.LayoutFile);
      end
    end
    
    
  end
  
  methods (Static)
  end
  
end

