function [joules,cardinality]=estimateEnergyConsumption(numbers,agents,diagonalUnitLength,filamentStepSize,numberOfMotorsNeeded)
%estimateEnergyConsumption estimate how much energy a certain number of
%agnts would require to solve a given subset sum network. 

if nargin<1
  numbers=randi(1000,1,40);
end
if nargin<2
  agents=randi(1000,1,40);
end
if nargin<3
  diagonalUnitLength=5.415; %for the actin device, for the MT device it is 10.6398
end
if nargin<4
  filamentStepSize=0.02; %in �m; default is set for actin
end
if nargin<5
  numberOfMotorsNeeded=5;
end
atpEnergy=30000; %in kJ/mol;
Na=6.022E23; %molecules per mol
energyPerStep=atpEnergy/Na;
problemSize=length(numbers);
joules=zeros(1,problemSize-1);
cardinality=2:problemSize;
for n=2:problemSize
  joules(n-1)=sum(numbers(1:n))*diagonalUnitLength/filamentStepSize*numberOfMotorsNeeded.*agents(n-1)*energyPerStep;
end