classdef SATClass < handle
  %SATClass simulates solving SAT problems with network biocomputation
  %devices
  %
  
  properties
    Problem = []; % Problem is a m by n sparse matrix, where m is the number of clauses and n is the number of variables. Example: [ 1 0 -1; -1 1 0] represents the problem (x1 or not x3) and (not x1 or x2).
    NVars; % Number of variables in the problem.
    NClauses; % Number of clauses in the problem.
    K = 3; % Maximum number of variables per clause for k-SAT
    KMin = 3; % Minimum number of variables per clause
    Solution = []; % A matrix where each row corresponds to a solution for the problem. If there is no solution it is set to NaN. If it is empty, run solveBruteForce to find solutions.
    SplitRatio = 0.5;
    PickupProb = 1;
    SavePath = 'results';
    SaveName;
    SpaceSATObj;
  end
  
  methods
    
    
    function SAT = SATClass(varargin)
      if nargin > 0
        p=inputParser;
        p.addParameter('Solvable', [], @isnumeric);
        p.addParameter('MakeSpaceSAT', false, @islogical);
        p.KeepUnmatched=true;
        
        p.parse(varargin{:});
        
        Names=fieldnames(p.Unmatched);
        for i=1:length(Names)
          if isempty(SAT.findprop(Names{i}))
            warning(strcat('SATClass does not support the parameter: ',Names{i}));
          else
            SAT.(Names{i})=p.Unmatched.(Names{i});
          end
        end
        if SAT.K > SAT.NVars
          SAT.K = SAT.NVars;
        end
        if SAT.KMin > SAT.K
          SAT.KMin = SAT.K;
        end
        if isempty(SAT.Problem)
          if isempty(SAT.NClauses) && SAT.NVars < 4
            SAT.makeMinimalExample('Solvable', p.Results.Solvable);
          else
            SAT.makeRandomExample('Solvable', p.Results.Solvable);
          end
        end
        SAT.NVars = size(SAT.Problem,2);
        SAT.NClauses = size(SAT.Problem,1);
        ProbName = SAT.printProblem;
        if length(ProbName) > 7
          SAT.SaveName = ProbName(7:end);
        end
        if SAT.NVars < 30 && isempty(SAT.Solution)
          SAT.solveBruteForce;
        end
        if p.Results.MakeSpaceSAT
          SAT.SpaceSATObj = SpaceSATClass(SAT.Problem);
          SAT.SpaceSATObj.AnnotationText = [{SAT.printProblem('Latex', true)}, {SAT.printSolution('Latex', true)}];
        end
      end
    end
    
    
    function SAT = makeMinimalExample(SAT, varargin)
      p = inputParser;
      p.addParameter('Solvable', [], @isnumeric);
      p.parse(varargin{:});
      if isempty(SAT.NClauses)
        SAT.NClauses = 2 ^ SAT.NVars;
        if p.Results.Solvable > 0
          SAT.NClauses = SAT.NClauses - 1;
        end
      end
      SAT.Problem = zeros(SAT.NClauses, SAT.NVars);
      for n = 0:SAT.NClauses - 1
        Binary = dec2bin(n,SAT.NVars);
        Assignment = Binary - '0';
        Assignment(Assignment == 0) = -1;
        SAT.Problem(n+1,:) = Assignment;
      end
    end
    
    
    function SAT = makeRandomExample(SAT, varargin)
      p = inputParser;
      p.addParameter('Solvable', [], @isnumeric); % if empty, we don't care about the number of solutions. if it is a number, the number of solutions should be equal to that number if it is infinite, then there should be a solution but we don't care about how many solutions there are)
      p.parse(varargin{:});
      if isempty(SAT.NVars)
        SAT.NVars = randi(17)+3;
      end
      if isempty(SAT.NClauses)
        SAT.NClauses = round(SAT.NVars * (3+rand(1)));
      end
      SafeCounter = 0;
      if isempty(p.Results.Solvable)
        MaxRuns = 1;
        DontCareAboutSolution = true;
      else
        if SAT.NVars > 21
          MaxRuns = 1;
          DontCareAboutSolution = true;
          warning('Randomly finding problems with more than 21 vars would take too long. Just creating one random example.')
        else
          MaxRuns = 100*SAT.NClauses*2^SAT.NClauses;
        DontCareAboutSolution = false;
        % initialize SAT.Solution with the opposite of the desired solution
        % to make sure that at least one problem is generated in the while
        % loop
        if p.Results.Solvable > 0
          SAT.Solution = NaN;
        else
          SAT.Solution = 0;
        end
        end
      end
      while SafeCounter < MaxRuns && (DontCareAboutSolution || ~(...
          ((p.Results.Solvable == 0) && all(isnan(SAT.Solution(:)))) || ...
          (p.Results.Solvable == size(SAT.Solution, 1) && ~all(isnan(SAT.Solution(:)))) || ...
          (isinf(p.Results.Solvable) && ~all(isnan(SAT.Solution(:))))...
          ))
        SAT.Problem = randi([-1,1], SAT.NClauses, SAT.NVars);
        VarsPerClause = sum(SAT.Problem ~= 0, 2);
        if max(VarsPerClause) < SAT.K
          while nnz(SAT.Problem(1,:)) < SAT.K
            SAT.Problem(1,randi(SAT.NVars)) = randi([-1,1]);
          end
          VarsPerClause(1) = SAT.K;
        end
        for n = find(VarsPerClause < SAT.KMin)'
          while nnz(SAT.Problem(n,:)) < SAT.KMin
            SAT.Problem(n,randi(SAT.NVars)) = randi([-1,1]);
          end
        end
        for n = find(VarsPerClause > SAT.K)'
          while nnz(SAT.Problem(n,:)) > SAT.K
            SAT.Problem(n,randi(SAT.NVars)) = 0;
          end
        end
        if SAT.NVars < 22
          SAT.solveBruteForce('Verbose', false);
        end
        SafeCounter = SafeCounter + 1;
        if mod(SafeCounter, MaxRuns/10) == 0
          fprintf('Generated %d problems, % 3.0f %% tries left\n', SafeCounter, (1 - SafeCounter / MaxRuns) * 100);
        end
      end
      if SafeCounter > 1 && SafeCounter >= MaxRuns
        error('You are trying to randomly generate a problem that should have %d solutions. I tried %d times to get it right and then gave up. Please try different paramters',p.Results.Solvable, SafeCounter);
      end
    end
    
    
    function SAT = setProblem(SAT, Problem)
      SAT.Problem = Problem(:,:);
      SAT.NVars = size(SAT.Problem,2);
      SAT.NClauses = size(SAT.Problem,1);
    end
    
    
    function Result = testAssignment(SAT,Assignment)
      %testAssignment tests whether a certain combination of true and false
      %input assignments returns true when applied to the SAT problem.
      
      if length(Assignment) ~= SAT.NVars
        error('MATLAB:biocomp_sim:SATClass:testVariables:varNum','The number of variables (%d) must be equal to the number of assignments: %d.',SAT.NVars,length(Assignment));
      end
      ResultClauses = (SAT.Problem .* Assignment(ones(1,size(SAT.Problem,1)),:)) > 0;
      Result = all(any(ResultClauses,2));
    end
    
    
    function SAT = solveBruteForce(SAT,varargin)
      %solveBruteForce tries all possible assignments and stores solutions
      %in the Solution property. If no solution is found, Solution is set
      %to NaN.
      
      p=inputParser;
      p.addParameter('Verbose', true, @islogical);
      p.parse(varargin{:});
      Brutef = tic;
      SAT.Solution = [];
      NTries = 2 ^ SAT.NVars - 1;
      if isempty(gcp('nocreate'))
        for n = 0:NTries
          Binary = dec2bin(n,SAT.NVars);
          Assignment = Binary - '0';
          Assignment(Assignment == 0) = -1;
          if SAT.testAssignment(Assignment)
            SAT.Solution(end + 1, :) = Assignment;
          end
        end
      else
        Soln=zeros(NTries, SAT.NVars);
        NV = SAT.NVars;
        parfor n = 1:NTries+1
          Binary = dec2bin(n-1, NV);
          Assignment = Binary - '0';
          Assignment(Assignment == 0) = -1;
          if SAT.testAssignment(Assignment) %#ok<PFBNS>
            Soln(n, :) = Assignment;
          end
        end
        Soln(sum(Soln,2)==0,:)=[];
        SAT.Solution = Soln;
      end
      if isempty(SAT.Solution)
        SAT.Solution = NaN;
      end
      if p.Results.Verbose
        toc(Brutef);
      end
    end
    
    
    function Results = simulate(SAT, varargin)
      %simulate perform monte-carlo simulation of agents moving through the
      %network and picking up tags at the clauses.
      
      p=inputParser;
      p.addParameter('Agents', round(2 ^ SAT.NVars * 0.8 * SAT.NVars), @isnumeric);
      p.addParameter('SimulatePickup', false, @islogical);
      p.addParameter('AdaptRatioToProblem', false, @islogical);
      p.addParameter('Verbose', true, @islogical);
      p.parse(varargin{:});
      Sim = tic;
      Results = zeros(p.Results.Agents, SAT.NClauses);
      if isempty(gcp('nocreate'))
        for n = 1:SAT.NVars
          Agents = rand(p.Results.Agents, 1);
          if p.Results.AdaptRatioToProblem
            Clauses = nnz(SAT.Problem(:, n));
            TrueClauses = sum(SAT.Problem(:, n) > 0);
            AgentsTrue = Agents > 1 - (TrueClauses / Clauses);
          else
            AgentsTrue = Agents > SAT.SplitRatio;
          end
          AgentsFalse = ~AgentsTrue;
          VarClauses = SAT.Problem(:,n)';
          if p.Results.SimulatePickup && SAT.PickupProb < 1
            Pickup = rand(p.Results.Agents, SAT.NClauses) .* VarClauses(ones(p.Results.Agents, 1), :);
          else
            Pickup = VarClauses(ones(p.Results.Agents, 1), :);
          end
          Results(AgentsTrue,:) = Results(AgentsTrue, :) | (Pickup(AgentsTrue, :) > (1 - SAT.PickupProb));
          Results(AgentsFalse,:) = Results(AgentsFalse, :) | (Pickup(AgentsFalse, :) < (-1 + SAT.PickupProb));
          if p.Results.Verbose
            fprintf('Simulated %d of %d variables.\n', n, SAT.NVars);
          end
        end
      else
        SimPick = p.Results.SimulatePickup && SAT.PickupProb < 1;
        Adapt = p.Results.AdaptRatioToProblem;
        Ping = round(p.Results.Agents/10);
        SplitRatios = (sum(SAT.Problem < 0) ./ sum(SAT.Problem ~= 0));
        Talk = p.Results.Verbose;
        parfor i=1:p.Results.Agents
          if Adapt
            Direction = rand(1,SAT.NVars) > SplitRatios; %#ok<PFBNS>
          else
            Direction = rand(1,SAT.NVars) > SAT.SplitRatio;
          end
          if SimPick
            Pickup = rand(SAT.NClauses, SAT.NVars) .* SAT.Problem;
          else
            Pickup = SAT.Problem;
          end
          Res = zeros(SAT.NClauses, SAT.NVars);
          Res(:,Direction) = Pickup(:,Direction) > (1 - SAT.PickupProb);
          Res(:,~Direction) = Pickup(:,~Direction) < (-1 + SAT.PickupProb);
          Results(i,:) = any(Res,2)';
          if Talk && mod(i,Ping) == 0
            fprintf('Finished simulating a 10%% chunk of agents. Elapsed time: %.0f s.\n',toc(Sim));
          end
        end
      end
      if p.Results.Verbose
        toc(Sim);
      end
    end
    
    
    function [Stats, SAT] = compareAdaptiveStatic(SAT, varargin)
      p=inputParser;
      p.addParameter('Problems', 1, @isnumeric);
      p.addParameter('SimulationRuns', 6, @isnumeric);
      p.addParameter('Verbose', true, @islogical);
      p.KeepUnmatched = true;
      p.parse(varargin{:});
      Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
      SimArgs = reshape(Tmp',[],1)';
      SimArgs = [{'Verbose',false} SimArgs];
      Compare = tic;
      Stats = zeros(p.Results.Problems,3,p.Results.SimulationRuns);
      for n = 1:p.Results.Problems
        if n > 1
          SAT.makeRandomExample;
          SAT.solveBruteForce('Verbose',p.Results.Verbose);
        end
        if isnan(SAT.Solution)
          Stats(n,1,:)=NaN(1,1,p.Results.SimulationRuns);
        else
          Stats(n,1,:)=repmat(size(SAT.Solution,1),1,1,p.Results.SimulationRuns);
        end
        for i = 1:p.Results.SimulationRuns
          Results = SAT.simulate(SimArgs{:});
          Stats(n,2,i) = sum(sum(Results, 2) == SAT.NClauses);
          Results = SAT.simulate('AdaptRatioToProblem', true,SimArgs{:});
          Stats(n,3,i) = sum(sum(Results, 2) == SAT.NClauses);
          Complete = ((n-1) * p.Results.SimulationRuns + i) / (p.Results.Problems*p.Results.SimulationRuns);
          Elapsed = toc(Compare);
          ETR = (1 / Complete) * Elapsed - Elapsed;
          if p.Results.Verbose
            fprintf('%d %% complete. ETR: %.0f s remaining.\n',round(Complete*100),ETR);
          end
        end
      end
      if p.Results.Verbose
        toc(Compare)
      end
      fprintf('Mean improvement: %.1f ? %.2f times more solutions found by adaptive network.\n',nanmean(nanmean(Stats(:,3,:)./Stats(:,2,:),3)),nanstd(nanmean(Stats(:,3,:)./Stats(:,2,:),3)));
    end
    
    
    function plotStats(SAT, Stats)
      if nargin < 2
        Stats = SAT.compareAdaptiveStatic;
      end
      Fig = createBasicFigure('Width',4,'Aspect',2/4);
      Ax = gca;
      Ax.FontSize = 12;
      bar(1:2,mean(Stats(1,2:3,:),3),0.8);
      hold on;
      errorbar(1:2,mean(Stats(1,2:3,:),3),std(Stats(1,2:3,:),1,3),'LineStyle','none');
      ylabel('Number of correct results','FontSize',14);
      xlim([0.4 2.6]);
      set(Ax,'XTickLabelMode','manual','XTickLabel',{'even','adaptive'},'XTickLabelRotation',90);
      if ispc
        FileNameLimit = 150;
      else
        FileNameLimit = 251;
      end
      saveas(Fig,fullfile(SAT.SavePath,[ellipsize(sprintf('SATClass%s.mat',SAT.SaveName),FileNameLimit), '.pdf']),'pdf');
      close(Fig);
    end
    
    
    function [Stats, SAT] = multiCompareAdaptiveStatic(SAT, MaxVars, varargin)
      p=inputParser;
      p.KeepUnmatched = true;
      p.parse(varargin{:});
      Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
      SimArgs = reshape(Tmp',[],1)';
      
      Stats = cell(1,MaxVars - 2);
      for n = 1:MaxVars - 2
        SAT.NVars = n + 2;
        SAT.NClauses = round(SAT.NVars * 3.5);
        SAT.makeRandomExample;
        SAT.solveBruteForce('Verbose',false);
        Stats{n} = SAT.compareAdaptiveStatic('Verbose', false, SimArgs{:});
      end
    end
    
    
    function Text = printProblem(SAT,varargin)
      %printProblem writes out the problem in human readable form.
      
      p = inputParser;
      p.addParameter('LaTex',false,@islogical);
      p.parse(varargin{:});
      if p.Results.LaTex
        AndText = ' \land ';
        Text = '$\Phi = ';
      else
        AndText = ' and ';
        Text = 'Phi = ';
      end
      for n = 1:size(SAT.Problem,1)
        Text = [Text '(' SAT.printVariables(SAT.Problem(n,:),'LaTex',p.Results.LaTex) ')' AndText]; %#ok<AGROW>
      end
      if p.Results.LaTex
        Text = [Text(1:end-7) '$'];
      else
        Text = Text(1:end-5);
      end
    end
    
    
    function Text = printSolution(SAT,varargin)
      p = inputParser;
      p.addParameter('LaTex',false,@islogical);
      p.parse(varargin{:});
      if p.Results.LaTex
        Pattern = '%s $x_%d = %s$,';
      else
        Pattern = '%s x%d = %s,';
      end
      if isempty(SAT.Solution)
        Text = 'unknown';
      elseif isnan(SAT.Solution)
        Text = 'no-solution';
      else
        Text='';
        for n=1:length(SAT.Solution)
          if SAT.Solution(n) > 0
            T = 'T';
          else
            T = 'F';
          end
          Text = sprintf(Pattern, Text, n, T);
        end
      end
    end
    
    
    function drawNetwork(SAT,varargin)
      %drawNetwork draws the biocomputation network corresponding to the
      %problem
      
      p=inputParser;
      p.KeepUnmatched=true;
      p.parse(varargin{:});
      
      MaxClausesPerVar = max(sum(abs(SAT.Problem),1));
      GraphSize = [MaxClausesPerVar, SAT.NVars + 1];
      LineHeight = 0.9/GraphSize(2);
      ColumnWidth = 0.85/GraphSize(1);
      fig1 = createBasicFigure();
      set(fig1,'Units','normalized','Visible','on');
      annotation(fig1,'textbox',[0.45 0.95 0.1 0.03],'String', 'entrance', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');
      annotation(fig1,'textbox',[0.45 0.02 0.1 0.03],'String', 'exit', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');
      annotation(fig1,'arrow',[0.5 0.05], [0.95, 0.95 - 0.8 * LineHeight],'Color','blue');
      annotation(fig1,'line',[0.5 0.95], [0.95, 0.95 - 0.8 * LineHeight],'Color','blue');
      annotation(fig1,'arrow',[0.95 0.9], [0.95 - 0.8 * LineHeight, 0.95 - 1.2 * LineHeight],'Color','blue');
      for n = 1:SAT.NVars
        YTrue = 0.95 - n * LineHeight + 0.2 * LineHeight;
        YFalse = 0.95 - n * LineHeight - 0.2 * LineHeight;
        X = 0.05;
        UsedClauses = find(SAT.Problem(:,n));
        for i = 1: length(UsedClauses)
          if SAT.Problem(UsedClauses(i),n)>0
            annotation(fig1,'arrow',[X, X + 0.25 * ColumnWidth], [YTrue, YTrue - 0.1 * LineHeight]);
            annotation(fig1,'textbox',...
              [X + 0.25 * ColumnWidth YTrue - 0.3 * LineHeight 0.5 * ColumnWidth 0.2 * LineHeight],...
              'String', num2str(UsedClauses(i),'C%d'), 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');
            annotation(fig1,'arrow',[X + 0.75 * ColumnWidth, X + ColumnWidth], [YTrue - 0.1 * LineHeight, YTrue]);
          else
            annotation(fig1,'arrow',[X, X + ColumnWidth], [YTrue, YTrue]);
          end
          X = X + ColumnWidth;
        end
        if (X + 0.05 * ColumnWidth) < 0.9
          annotation(fig1,'arrow',[X, 0.9], [YTrue, YTrue]);
          annotation(fig1,'arrow',[0.9 X], [YFalse, YFalse]);
        end
        for i = length(UsedClauses):-1:1
          if SAT.Problem(UsedClauses(i),n)<0
            annotation(fig1,'arrow',[X, X - 0.25 * ColumnWidth], [YFalse, YFalse + 0.1 * LineHeight]);
            annotation(fig1,'textbox',...
              [X - 0.75 * ColumnWidth, YFalse + 0.1 * LineHeight 0.5 * ColumnWidth 0.2 * LineHeight],...
              'String', num2str(UsedClauses(i),'C%d'), 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');
            annotation(fig1,'arrow',[X - 0.75 * ColumnWidth, X - ColumnWidth], [YFalse + 0.1 * LineHeight, YFalse]);
          else
            annotation(fig1,'arrow',[X, X - ColumnWidth], [YFalse, YFalse]);
          end
          X = X - ColumnWidth;
        end
        if n ~= SAT.NVars
          %arrows connecting the rows:
          annotation(fig1,'arrow',[0.95, 0.05], [YFalse, 0.95 - (n + 1) * LineHeight + 0.2 * LineHeight],'Color','blue');
          annotation(fig1,'line',[0.9, 0.95], [YTrue, YFalse],'Color','blue');
          annotation(fig1,'arrow',[0.95, 0.95], [YFalse, 0.95 - (n + 1) * LineHeight + 0.2 * LineHeight],'Color','blue');
          annotation(fig1,'arrow',[0.95, 0.9], [0.95 - (n + 1) * LineHeight + 0.2 * LineHeight, 0.95 - (n + 1) * LineHeight - 0.2 * LineHeight],'Color','blue');
          annotation(fig1,'arrow',[0.05, 0.05], [YFalse, 0.95 - (n + 1) * LineHeight + 0.2 * LineHeight],'Color','blue');
          annotation(fig1,'arrow',[0.05, 0.95], [YFalse, 0.95 - (n + 1) * LineHeight + 0.2 * LineHeight],'Color','blue');
        end
      end
      annotation(fig1,'line',[0.9, 0.95], [0.05 + 1.2 * LineHeight, 0.05 + 0.8 * LineHeight],'Color','blue');
      annotation(fig1,'arrow',[0.95, 0.5], [0.05 + 0.8 * LineHeight, 0.05],'Color','blue');
      annotation(fig1,'arrow',[0.05, 0.5], [0.05 + 0.8 * LineHeight, 0.05],'Color','blue');
      saveas(fig1,fullfile(SAT.SavePath,['drawSATNetwork(' ellipsize(SAT.SaveName,140) ').pdf']),'pdf');
    end
    
    
    function writeDXF(SAT, varargin)
      %writeDXF writes the network into a dxf file that can be used to
      %create a layout.
      
      p=inputParser;
      % define some layout specific parameters
      p.addParameter('UnitCellXDim', 3.5, @isnumeric);
      p.addParameter('UnitCellYDim', 16.42, @isnumeric);
      p.addParameter('NetworkOrigin', [20 240], @(x) isnumeric(x) && length(x)==2);
      p.addParameter('LZOffset', [8.64 6.4186666667], @(x) isnumeric(x) && length(x)==2);
      p.addParameter('LabelTextOffset', [-30 305], @(x) isnumeric(x) && length(x)==2);
      p.addParameter('LabelFontSize', 1.5, @isnumeric);
      p.addParameter('LayoutName', inputname(1), @ischar);
%       p.addParameter('Scale',1.5,@isnumeric);
      p.addParameter('BaseLayout', 'Base_layout_SAT_final_for_simulation.DXF', @ischar);
      p.addParameter('SavePath', SAT.SavePath, @ischar);
      p.KeepUnmatched=true;
      p.parse(varargin{:});
      Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
      PassthroughArgs = reshape(Tmp',[],1)';
      if ~isempty(strfind(p.Results.BaseLayout,'for_simulation'))
        NameDistinction = '_sim';
      else
        NameDistinction = '_fab';
      end
      DXFW = DXFWriterClass(fullfile(p.Results.SavePath,['SAT' NameDistinction ellipsize(SAT.SaveName,147) '.DXF']),...
        'BaseLayout', p.Results.BaseLayout, PassthroughArgs{:});
      DXFW.writeDXFHeader;
      LZOff = p.Results.NetworkOrigin + p.Results.LZOffset;
      DXFW.makeInsert('LZ',LZOff(1),LZOff(2));
      DXFW.makeInsert('LZ', LZOff(1) - 2 * p.Results.LZOffset(1) + p.Results.UnitCellXDim,...
        LZOff(2), 'FlipX', true);
      X = p.Results.NetworkOrigin(1);
      Y = p.Results.NetworkOrigin(2);
      DXFW.annotateDXF(p.Results.LayoutName, X + p.Results.LabelTextOffset(1), Y + p.Results.LabelTextOffset(2),0,[0 0],p.Results.LabelFontSize);
      InsertName = 'split';
      for n = 1:SAT.NVars
        if n == SAT.NVars
          InsertName = 'end_funnel';
        end
        DXFW.makeInsert(InsertName,X,Y);
        SAT.addDXFClauseLabels(n, DXFW, X, Y,...
          'UnitCellXDim', p.Results.UnitCellXDim, 'UnitCellYDim', p.Results.UnitCellYDim);
        if ~isempty(strfind(p.Results.BaseLayout,'for_simulation'))
          SAT.addDXFCountingRegions(DXFW, n, X, Y);
        end
        Y = Y - p.Results.UnitCellYDim;
      end
    end
    
    
    function SAT = addDXFCountingRegions(SAT, DXFW, n, X, Y, varargin)
      p = inputParser;
      p.addParameter('CountingOffset', [-0.4, 2.35, 5], @(x) isnumeric(x) && length(x)==3); %offset of the counting regions: [x1, x2, y] x1 and x2 are the respective x offsets of counting region 1 and 2
      p.addParameter('CountingDim', [1.5, 1], @(x) isnumeric(x) && length(x)==2); % Dimesions of counting region: [width height]
      p.parse(varargin{:});
      LayerName = SAT.makeLayerName(2 * n - 1,'Counting');
      RectX = [0, p.Results.CountingDim(1), p.Results.CountingDim(1), 0, 0];
      RectY = [0, 0, p.Results.CountingDim(2), p.Results.CountingDim(2), 0];
      DXFW.makeLWPolyLine(RectX + (X + p.Results.CountingOffset(1)),RectY + (Y + p.Results.CountingOffset(3)), LayerName);
      LayerName = SAT.makeLayerName(2 * n,'Counting');
      RectX = [0, p.Results.CountingDim(1), p.Results.CountingDim(1), 0, 0];
      RectY = [0, 0, p.Results.CountingDim(2), p.Results.CountingDim(2), 0];
      DXFW.makeLWPolyLine(RectX + (X + p.Results.CountingOffset(2)),RectY + (Y + p.Results.CountingOffset(3)), LayerName);
    end
    
    
    function addDXFClauseLabels(SAT, VarNo, DXFW, X, Y, varargin)
      p=inputParser;
      p.addParameter('UnitCellXDim', 3.5, @isnumeric);
      p.addParameter('UnitCellYDim', 6.42, @isnumeric);
      p.addParameter('ClauseTextOffset', [0 4], @(x) isnumeric(x) && length(x)==2);
      p.addParameter('ClauseSpacing', 3, @(x) isnumeric(x) && length(x)==1);
      p.parse(varargin{:});
      Y = Y + 2/3 * p.Results.UnitCellYDim;
      X0 = X;
      TrueClauses = find(SAT.Problem(:,VarNo) > 0);
      for n = 1: length(TrueClauses)
        Text = sprintf('C%d',TrueClauses(n));
        X = X - (p.Results.ClauseSpacing + 2.3 * length(Text));
        DXFW.annotateDXF(Text,X,Y);
      end
      X = X0 + p.Results.UnitCellXDim + p.Results.ClauseSpacing;
      FalseClauses = find(SAT.Problem(:,VarNo) < 0);
      for n = 1:length(FalseClauses)
        Text = sprintf('C%d',FalseClauses(n));
        DXFW.annotateDXF(Text,X,Y);
        X = X + (p.Results.ClauseSpacing + 2.3 * length(Text));
      end
    end
    
    
    function [FilamentTags,Success] = evaluateFilamentCounts(SAT, FilamentCounts)
      NFilaments = length(FilamentCounts);
      FilamentTags = cell(NFilaments,1);
      Success = false(NFilaments,1);
      if iscell(FilamentCounts)
        for n = 1:NFilaments
          Variables = ceil(FilamentCounts{n} ./ 2);
          Assignment = mod(FilamentCounts{n}, 2);
          Assignment(Assignment==0) = -1;
          FilamentTags{n} = [];
          if ~isempty(Assignment)
            for i = 1:length(FilamentCounts{n})
              FilamentTags{n} = [FilamentTags{n}; find(SAT.Problem(:,Variables(i))==Assignment(i))];
            end
            if length(Assignment) == SAT.NVars
              Success(n) = SAT.testAssignment(Assignment(Variables));
            else
              Success(n) = NaN;
            end
          end
        end
      else
        error('FilamentCounts must be a cell');
      end
    end
    
    
    function [Fig, Axes] = makeSpaceCombinedPlot(SAT, varargin)
        [Fig, Axes] = SAT.callSpaceSATCommand('makeCombinedPlot', inputname(1), varargin{:});
    end
    
    
    function [Fig, Axes] = drawSpaceNetwork(SAT, varargin)
        [Fig, Axes] = SAT.callSpaceSATCommand('drawExcovNetwork', inputname(1), varargin{:});
    end
    
    
    function [Fig, Axes] = simulateSpaceNetwork(SAT, varargin)
        [Fig, Axes] = SAT.callSpaceSATCommand('SubSumNetworkObj.multisim', inputname(1), varargin{:});
    end

    
    function [Fig, Axes] = plotErrorDistribution(SAT, varargin)
        [Fig, Axes] = SAT.callSpaceSATCommand('SubSumNetworkObj.plotErrorDistribution', inputname(1), varargin{:});
    end
    

    function writeSpaceDXF(SAT, varargin)
      %writeSpaceDXF writes the space encoded SAT network into a dxf file 
      %that can be used to create a layout.
      %
      % See also: SubSumNetworkClass.writeDXF
      
      p=inputParser;
      % define some layout specific parameters
      p.addParameter('LayoutName',inputname(1),@ischar);
      p.KeepUnmatched=true;
      p.parse(varargin{:});
      Tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
      PasstrhoughArgs = reshape(Tmp',[],1)';
      if isempty(SAT.SpaceSATObj.SubSumNetworkObj)
        SAT.SpaceSATObj.createSubSumNetwork;
      end
      SAT.SpaceSATObj.SubSumNetworkObj.SaveName = SAT.SpaceSATObj.SubSumNetworkObj.makeAgentName(SAT.SpaceSATObj.SubSumNetworkObj.SaveName,p.Results,{'LayoutName'});
      SAT.SpaceSATObj.SubSumNetworkObj.writeDXF('LayoutName', p.Results.LayoutName, PasstrhoughArgs{:});
    end
    
    
    function save(SAT)
      if ispc
        FileNameLimit = 150;
      else
        FileNameLimit = 251;
      end
      save(fullfile(SAT.SavePath,[ellipsize(sprintf('SATClass%s.mat',SAT.SaveName),FileNameLimit), '.mat']),'SAT','-v7.3');
    end
    
    
  end
  
  
  methods (Access = private)
      
      
    function [Fig, Axes] = callSpaceSATCommand(SAT, Command, Name, varargin)
      p=inputParser;
      p.addParameter('Save', true, @islogical);
      p.KeepUnmatched = true;
      p.parse(varargin{:});
      Tmp = [fieldnames(p.Unmatched), struct2cell(p.Unmatched)];
      Passthrough = [{'Save', false} reshape(Tmp', [], 1)'];
      if length(Command) > 17 && strcmp(Command(1:17), 'SubSumNetworkObj.')
        Command = Command(18:end);
        if isempty(SAT.SpaceSATObj.SubSumNetworkObj)
            SAT.SpaceSATObj.createSubSumNetwork();
        end
        [Fig, Axes,~,~,AgentName] = SAT.SpaceSATObj.SubSumNetworkObj.(Command)(Passthrough{:});
      else
        [Fig, Axes, AgentName] = SAT.SpaceSATObj.(Command)(Passthrough{:});
      end
      FieldsToClean={'Save'};
      AgentName=SAT.SpaceSATObj.SubSumNetworkObj.makeAgentName(AgentName,p.Results,unique([p.UsingDefaults, FieldsToClean]));
      AgentName=regexprep(AgentName,'_YScale-1','');
      if p.Results.Save
        if strcmp(Command, 'makeCombinedPlot')
            Command = 'mCP';
        elseif strcmp(Command, 'plotErrorDistribution')
            Command = 'pED';
        end
        Fig.Renderer = 'Painters';
        saveas(Fig,fullfile(SAT.SavePath, [ellipsize( [Name '_' Command '(' AgentName ')'], 251) '.pdf']));
        close(Fig);
      else
        set(Fig,'Visible','on');
      end
    end
    
    
  end
  
  
  methods (Static)
    
    
    function LayerName = makeLayerName(n,Name)
      LayerNo = floor(n / 10);
      LayerDataType = mod(n, 10);
      LayerName = sprintf('L%dD%d_%s', LayerNo, LayerDataType, Name);
    end
    
    
    function Text = printVariables(Variables,varargin)
      p = inputParser;
      p.addParameter('LaTex',false,@islogical);
      p.parse(varargin{:});
      UsedVars = find(Variables);
      Text = '';
      for i = 1:length(UsedVars)
        if p.Results.LaTex
          if Variables(UsedVars(i)) < 0
            Text = [Text num2str(UsedVars(i),'\\lnot x_%d \\lor') ' ']; %#ok<AGROW>
          else
            Text = [Text num2str(UsedVars(i),'x_%d \\lor') ' ']; %#ok<AGROW>
          end
        else
          if Variables(UsedVars(i)) < 0
            Text = [Text num2str(UsedVars(i),'~x%d or') ' ']; %#ok<AGROW>
          else
            Text = [Text num2str(UsedVars(i),'x%d or') ' ']; %#ok<AGROW>
          end
        end
      end
      if p.Results.LaTex
        Text = Text(1:end-6);
      else
        Text = Text(1:end-4);
      end
    end
    
    
  end
  
end

