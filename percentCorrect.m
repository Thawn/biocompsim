function percentage=percentCorrect(numbers,errorOP)
%percentCorrect estimates the number of agents expected to pass a certain
%subset sum networ (defined by the vector numbers) without making an error.

opJunctions=sum(numbers-1);
percentage=(1-errorOP)^opJunctions;