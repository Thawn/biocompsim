function [Times, Cardinality, A, B]=extrapolateComputingTime(ProblemSize,varargin)
p=inputParser;
p.addParameter('Factor', 1.554e-07, @isnumeric); %2013 macbook pro:6.111e-07, 2017 macbook pro:1.554e-07
p.addParameter('Exponent', 0.8212, @isnumeric); %2013 macbook pro :0.7801, 2017 macbook pro:0.8212
p.addParameter('Resolution', 0.1, @isnumeric);
p.addParameter('TimeUnit', 's', @ischar);
p.addParameter('CompTimesMeasured', [], @isnumeric);
p.parse(varargin{:})
A = p.Results.Factor;
B = p.Results.Exponent;
if ~isempty(p.Results.CompTimesMeasured)
  Fitobj = fit((2:length(p.Results.CompTimesMeasured) + 1)',p.Results.CompTimesMeasured','exp1');
  A = Fitobj.a;
  B = Fitobj.b;
end
switch p.Results.TimeUnit
  case 'm'
    TimeCor=60;
  case 'h'
    TimeCor=3600;
  otherwise
    TimeCor=1;
end 
Cardinality=2:p.Results.Resolution:ProblemSize;
Times=A.*exp(B.*Cardinality)./TimeCor;

