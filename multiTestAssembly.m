function Result = multiTestAssembly(Runs,NOligos)
Result = zeros(Runs,1);
parfor n=1:Runs
  Result(n)=testAssembly(NOligos);
end