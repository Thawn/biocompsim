function [times,cardinality]=estimateCalculationTime(numbers,diagonalUnitLength,filamentSpeed,timeUnit)
%estimateCalculationTime estimate how long it would take a filament to move
%along the longest path through a subset sum network.

if nargin<1
  numbers=randi(1000,1,40);
end
if nargin<2
  diagonalUnitLength=5.415; %for the actin device, for the MT device it is 10.6398
end
if nargin<3
  filamentSpeed=5; %in �m/s; default is set for actin
end
if nargin<4
  timeUnit='s';
end
switch timeUnit
  case 'm'
    timeCor=60;
  case 'h'
    timeCor=3600;
  otherwise
    timeCor=1;
end 
problemSize=length(numbers);
times=zeros(1,problemSize-1);
cardinality=2:problemSize;
for n=2:problemSize
  times(n-1)=sum(numbers(1:n))*diagonalUnitLength/filamentSpeed/timeCor;
end