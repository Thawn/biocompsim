classdef SpaceSATClass < ExcovClass
  properties
    SubSumNumbersSmall = []; % The subset sum numbers corresponding to each variable being assigned the value that returns a smaller decimal number. These are automatically generated during class creation.
    OptimizedSubSumNumbersSmall = []; % Optimized small SubSumNumbers.
    ResetTrueJunctionMatrix = []; % The matrix defining where ForceAdd junctions need to be placed in the subset sum network to avoid carry over. Can be created by the makeResetFalseJunctionMatrix method (together with the ResetFalseJunctionMatrix property inherited from ExcovClass).
    TopHalfSplitJunctionMatrix = []; % The matrix defining where TopHalfSplit junctions (split junctions that act split junctions for the top entrance and as pass junctions for the left entrance) need to be placed in the subset sum network to avoid carry over. Can be created by the makeResetFalseJunctionMatrix method (together with the ResetFalseJunctionMatrix property inherited from ExcovClass).
    AllNumbers = []; %Matrix containing the subset sum numbers corresponding to the true assignments in the top row and the numbers corresponding to the false assignments in the bottom row.
  end
  methods
    
    
    function S = SpaceSATClass(Problem,varargin)
      if nargin > 0
        p=inputParser;
        p.addParameter('Optimize',true,@islogical);
        p.KeepUnmatched=true;
        
        p.parse(varargin{:});
        
        Names=fieldnames(p.Unmatched);
        for i=1:length(Names)
          if isempty(S.findprop(Names{i}))
            warning(strcat('SATClass does not support the parameter: ',Names{i}));
          else
            S.(Names{i})=p.Unmatched.(Names{i});
          end
        end
        S.OptimizeSubSumEntrances = false; % must be set to false because the entrance optimization algorithm does not work for SpaceSAT networks.
        S.Sets = Problem;
        NumVars = size(S.Sets, 1);
        S.TargetSum = 2 ^ NumVars - 1;
        S.Mapping = (1:NumVars)';
        if p.Results.Optimize
          S.optimizeSet;
        end
        S.findSubSumNumbers;
      end
    end
    
    
    function S = findSubSumNumbers(S)
      %findSubSumNumbers converts the binary numbers to decimals which will
      %be used for the subset sum calculation.
      %
      % See also: findOptimizedSubSumNumbers
      
      TrueClauses = bin2dec(num2str((S.Sets > 0)'))';
      FalseClauses = bin2dec(num2str((S.Sets < 0)'))';
      S.AllNumbers = [TrueClauses; FalseClauses];
      S.SubSumNumbers = max(S.AllNumbers, [], 1);
      S.SubSumNumbersSmall = min(S.AllNumbers, [], 1);
      S.findOptimizedSubSumNumbers
      S.makeResetFalseJunctionMatrix;
    end


    function S = optimizeSet(S)
      %optimizeSet find an optimal mapping between SAT variables and subset
      %sum numbers such that the total sum of the subest sum numbers is
      %minimal
      %
      % See also deOptimizeSet,findOptimizedSubSumNumbers
      
      %first we sort the sets by how often each variable appears in all
      %clauses
      [S.Mapping, ~] = S.sortSets(abs(S.Sets));
      TempMapping = S.Mapping;
      S.deOptimizeSet;
      S.Mapping = TempMapping; 
    end
    
    
    function S=findOptimizedSubSumNumbers(S)
      %findOptimizedSubSumNumbers finds the minimum set of subset
      %sum numbers necessary to solve the EXCOV problem
      %
      % findOptimizedSubSumNumbers() removes subset sum numbers larger than
      % the target sum stores the remaining numbers in 
      % ExcovClass.OptimizedSubSumNumbers. Also stores the entrances into
      % the subset sum network in ExcovClass.OptimizedEntrances
      % and the number of rows in the optimized subset sum problem in
      % ExcovClass.OptimizedSubSumRows.
      %
      % See also: deoptimizeSubsum,optimizeSet,deOptimizeSet
      
      [MaxEntrance, Index] = max(S.SubSumNumbers);
      S.OptimizedEntrances = [S.SubSumNumbersSmall(Index), MaxEntrance];
      S.OptimizedSubSumNumbers=S.SubSumNumbers;
      S.OptimizedSubSumNumbers(Index) = [];
      S.OptimizedSubSumNumbersSmall = S.SubSumNumbersSmall;
      S.OptimizedSubSumNumbersSmall(Index) = [];
      S.OptimizedSubSumRows=sum(S.OptimizedSubSumNumbers);
    end
    
    
    function S = makeResetFalseJunctionMatrix(S,varargin)
      %makeResetFalseJunctionMatrix creates SpaceSATClass.ResetFalseJunctionMatrix, a
      %matrix in which each row corresponds to a split junction row in the
      %subset sum network and each column corresponds to a column in that
      %split junction row. ResetFalse junctions are placed at positions where
      %SpaceSATClass.ResetFalseJunctionMatrix is one. Also creates
      %SpaceSATClass.ResetTrueJunctionMatrix, a matrix with the same
      %dimensions as SpaceSATClass.ResetFalseJunctionMatrix but instead of
      %
      % makeResetFalseJunctionMatrix(true) ExcovClass.ResetFalseJunctionMatrix is
      % created only for the minimal necessary (optimized) part of the
      % subset sum network
      % 
      % makeResetFalseJunctionMatrix(true) ExcovClass.ResetFalseJunctionMatrix is
      % created for the entire subset sum network between 0 and
      % ExcovClass.TargetSum
      %
      % See also: getCollisions,createSubSumNetwork,findOptimizedSubSumNumbers
      
      range = 0:S.TargetSum;
      NumVars = length(S.OptimizedSubSumNumbers);
      NumClauses = length(dec2bin(S.TargetSum));
      S.ResetFalseJunctionMatrix = zeros(NumVars, length(range));
      S.ResetTrueJunctionMatrix = zeros(NumVars, length(range));
      S.TopHalfSplitJunctionMatrix = zeros(NumVars, length(range));
      
      for n = 1:NumVars
        for i = 1:length(range)
          BinCol = logical((dec2bin(range(i), NumClauses))' - '0');
          BinNumLarge = logical((dec2bin(S.OptimizedSubSumNumbers(n), NumClauses))' - '0');
          BinNumSmall = logical((dec2bin(S.OptimizedSubSumNumbersSmall(n), NumClauses))' - '0');
          NewBinNumSmall = BinNumSmall;
          NewBinNumSmall(BinCol & BinNumSmall) = 0;
          NewNumSmall = bin2dec(num2str(NewBinNumSmall'));
          if any(BinCol & BinNumLarge)
            S.ResetFalseJunctionMatrix(n, i) = 1;
            NewBinNumLarge = BinNumLarge;
            NewBinNumLarge(BinCol & NewBinNumLarge) = 0;
            NewNumLarge = bin2dec(num2str(NewBinNumLarge'));
          else
            NewNumLarge = S.OptimizedSubSumNumbers(n);
          end
          if NewNumLarge > NewNumSmall
            S.ResetTrueJunctionMatrix(n,i) = S.OptimizedSubSumNumbers(n) - NewNumSmall;
            S.TopHalfSplitJunctionMatrix(n,i) = S.OptimizedSubSumNumbers(n) - NewNumLarge;
          else
            S.TopHalfSplitJunctionMatrix(n,i) = S.OptimizedSubSumNumbers(n) - NewNumSmall;
            S.ResetTrueJunctionMatrix(n,i) = S.OptimizedSubSumNumbers(n) - NewNumLarge;
          end
        end
      end
    end
    
    
    function E=createSubSumNetwork(E,varargin)
      %createSubSumNetwork creates a subset sum network that solves the
      %exact cover problem
      %
      % See also: SubSumNetworkClass,makeResetFalseJunctionMatrix
      
      if isempty(E.ResetFalseJunctionMatrix)
        E.makeResetFalseJunctionMatrix;
      end
      E.SubSumNetworkObj=SubSumNetworkClass(E.OptimizedSubSumNumbers,...
          'Entrances',E.OptimizedEntrances,...
          'TargetSum',E.TargetSum,...
          'OptimizeEntrances', E.OptimizeSubSumEntrances, ...
          'ResetFalseJunctionMatrix',E.ResetFalseJunctionMatrix,...
          'ResetTrueJunctionMatrix', E.ResetTrueJunctionMatrix,...
          'TopHalfSplitJunctionMatrix', E.TopHalfSplitJunctionMatrix,...
          'Sort','descend',...
          varargin{:});
      E.SubSumNetworkObj.createNetworkMap;
      E.SubSumNetworkObj.determineCorrectSums;
    end
    

  end
end