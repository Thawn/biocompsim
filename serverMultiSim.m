function serverMultiSim(ArgFile)
%serverMultiSim used to simulate subset sum networks on an HPC cluster.
%
% Arguments for the SubSumNetworkClass.multisim function are stored as Cell
% variable in ArgFile.

%#function SubSumNetworkClass
Args=load(ArgFile,'SSP','SimArgs');
Args.SSP.multisim(Args.SimArgs{:});
close all;
disp('Success!');
end
