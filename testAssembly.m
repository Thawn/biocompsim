function N = testAssembly(NOligos)
Result = zeros(NOligos,1);
N=0;
while sum(Result)<NOligos
  Result(ceil(rand(1,1) * NOligos)) = 1;
  N = N + 1;
end