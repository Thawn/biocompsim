function [h,p,chi2,dof]=chi2CompareSimExp(sim_res,exp_res,alpha)
%chi2CompareSimExp performs a chi squared test to check if the experimental
%results are significantly different from the simulation results
%
% h=chi2CompareSimExp(sim_res,exp_res) returns true if the results in the
% row vector exp_res are significantly different from the results in the 
% row vector sim_res at 95% confidence level.
% 
% h=chi2CompareSimExp(sim_res,exp_res,alpha) returns true if the results in the
% vector exp_res are significantly different from the results in the vector
% sim_res at the confidence level given in 1-alpha (alpha=0.05 corresponds
% to 95% confidence level).
% 
% [h,p,chi2,dof]=chi2CompareSimExp(sim_res,exp_res,alpha) in addition to the
% answer h, also returns the p value, the chi2 value, and the degrees of
% freedom
%
% See also: SubSumNetworkClass,ExcovClass

if nargin<3
  alpha=0.05;
end
all_res=[sim_res;exp_res];
expectation = sum(all_res,2)*sum(all_res)/sum(all_res(:));
chi2table = (all_res-expectation).^2./expectation;
chi2 = sum(chi2table(:));
dof=prod(size(all_res)-1);
p = 1-chi2cdf(chi2,dof);
h=p<=alpha;



