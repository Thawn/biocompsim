function E=makeExcovSets(varargin)
%makeExcovSets randomly create EXCOV sets until they meet certain
%requirements
%
% Configuration parameters:
%   makeExcovSets('HasErrors','no'); Whether we allow the excov network to
%   have carry over errors without noadd junctions
%   makeExcovSets('HasCover','yes'); Whether the excov network should have
%   a solution.
%   makeExcovSets('MaxRows',130,@isnumeric); Maximum number of rows in the
%   excov network.
%   makeExcovSets('MinSubSumNumbers',[]); Minimum number of subset sum
%   numbers in the subset sum network.
%   makeExcovSets('UpdateFrequency',2); How often to display status
%   messages.
% remaining parameters are passed down to ExcovClass.
%
% See also: ExcovClass, ExcovClass.makeOptimalSet

p=inputParser;
%define the parameters
p.addParameter('HasErrors','no',@ischar);
p.addParameter('HasCover','yes',@ischar);
p.addParameter('MaxRows',130,@isnumeric);
p.addParameter('MinSubSumNumbers',[],@isnumeric);
p.addParameter('UpdateFrequency',2,@isnumeric);
p.KeepUnmatched=true;
p.parse(varargin{:});
tmp = [fieldnames(p.Unmatched),struct2cell(p.Unmatched)];
excovArgs = reshape(tmp',[],1)';
if strcmp(p.Results.HasCover,'no')&&~isfield(p.Unmatched,'Satisfiable')
  excovArgs{end+1}='Satisfiable';
  excovArgs{end+1}=false;
end
done=false;
updateInterval=p.Results.UpdateFrequency;
times=zeros(updateInterval,1);
n=1;
progress='';
c=0;
s=0;
while ~done
  if n>updateInterval
    meanTime=mean(times);
    updateInterval=ceil(1/meanTime);
    times=zeros(updateInterval,1);
    strBs=repmat('\b',1,length(progress));
    progress=sprintf('%d sets generated. %d sets solved. So far, none matched requirements. Average elapsed time per try is %.3f seconds.\n',c, s, meanTime);
    fprintf([strBs progress]);
    n=1;
  end
  tStart=tic;
  E=ExcovClass(excovArgs{:});
  n=n+1;
  c=c+1;
  if E.OptimizedSubSumRows<=p.Results.MaxRows && ...
      (isempty(p.Results.MinSubSumNumbers) || length(E.OptimizedSubSumNumbers) >= p.Results.MinSubSumNumbers)
    if strcmp('no',p.Results.HasErrors) && strcmp('no',p.Results.HasCover);
      %if we want neither a solution nor a carry error, verification can be
      %done quickly by the subsum result
      valid=determineCorrectSums(E.SubSumNumbers);
      done=~valid(E.TargetSum+1);
    else
      if strcmp(p.Results.HasCover,'no')
        E.solveFast(false);
        numbers=E.SubSumNumbers;
        Entrances=numbers(E.Sets(1,:)==1);
        numbers=setdiff(numbers,Entrances);
        Entrances(Entrances<(E.TargetSum-sum(numbers)))=[];
        done=~isempty(Entrances) && strcmp(E.HasErrors,p.Results.HasErrors) && strcmp(E.HasCover,p.Results.HasCover);
      else
        E.solveBruteForce(false);
        done=strcmp(E.HasErrors,p.Results.HasErrors) && strcmp(E.HasCover,p.Results.HasCover);
      end
      s=s+1;
    end
  end
  times(n)=toc(tStart);
end
E.solveBruteForce;

