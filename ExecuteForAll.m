names = who;
if ~exist('fun', 'var') == 1
  fun = '.makeCombinedPlot';
  varargs = {'''NoAdd''','true'};
end
for n = 1:length(names)
  if isa(eval(names{n}), 'ExcovClass') || isa(eval(names{n}), 'SubSumNetworkClass')
    EvalStr = [names{n} fun];
    if exist('varargs', 'var') == 1
      EvalStr = [EvalStr '(' strjoin(varargs,',') ');']; %#ok<AGROW>
    end
    eval(EvalStr)
  end
end