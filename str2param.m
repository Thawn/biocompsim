function Structure=str2param(Structure)
%str2param converts a structure of parameters to proper param value pairs.
%Used by serverMultiMultiSim to properly parse paramters coming from the
%command line.
%
%See also: serverMultiMultiSim

  Fields=fieldnames(Structure);
  for nn=1:length(Fields)
    if strcmp(Structure.(Fields{nn})(1),'{')
      Split=strsplit(Structure.(Fields{nn})(2:end-1),';');
      if isempty(str2num(Split{1})) %#ok<*ST2NM>
        Structure.(Fields{nn})=Split;
      else
        Structure.(Fields{nn})=cellfun(@str2num,Split,'UniformOutput',false);
      end
    else
      if isempty(str2num(Structure.(Fields{nn})))
        if strcmp(Structure.(Fields{nn}),'[]')
          Structure.(Fields{nn})=[];
        end
      else
          Structure.(Fields{nn})=str2num(Structure.(Fields{nn}));
      end
    end
  end
end
