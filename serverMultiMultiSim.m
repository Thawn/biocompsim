function serverMultiMultiSim(SavePath,varargin)
%serverMultiMultiSim used to simulate subset sum networks on an HPC cluster
%using the multiMultiSim function.

Parameters=struct(varargin{:});
Parameters=str2param(Parameters);
Tmp = [fieldnames(Parameters),struct2cell(Parameters)];
SimArgs = reshape(Tmp',[],1)';
multiMultiSim('SavePath',SavePath,SimArgs{:});