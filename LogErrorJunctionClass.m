classdef LogErrorJunctionClass < JunctionClass
  %LogErrorJunctionClass class used to simulate the behavior of agents at
  %individual junctions within a subset sum network. Keeps track of how
  %many agents have made one or more errors and how many agents behaved
  %correctly.
  %
  % See also: SubSumNetworkClass,JunctionClass,DeterministicJunctionClass,CorrectedJunctionClass,CorrectedLogErrorJunctionClass
  
  methods
    
    
    function J = LogErrorJunctionClass(varargin)
      J@JunctionClass(varargin{:})
    end
    
    
    function [Output1, Output2, Error1, Error2] = calculate(J, InputB, InputA, JunctionType) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate calculates the behavior of agents in a junction also keeps
      %track of how many agents have performed illegal operations.
      %
      % Parameters:
      %   JunctionType: string, name of the junction, part of the function
      %   that will be called for the actual calculation.
      %   InputB: number of agents entering from the top.
      %   InputA: number of agents entering from the left.
      %
      % See also: calculateLineSegment, monteCarlo
      
      if nargin < 4
        JunctionType = 'Pass';
      end
      switch JunctionType
        case 'Split'
          [Output1, Output2, Error1, Error2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
          Output1 = Output1 + Error1;
          Output2 = Output2 + Error2;
          Error1=0;
          Error2=0;
        case 'SplitTop'
          [Output1, Output2, Error1, Error2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.ErrorOP);
          Output2 = Output2 + Error2;
          Error2=0;
        case 'SplitLeft'
          [Output1, Output2, Error1, Error2] = J.monteCarlo(InputB, InputA, J.ErrorOPB, J.RatioORA1);
          Output1 = Output1 + Error1;
          Error1=0;
        case 'ResetFalse'
          if J.DiscardReset
            Output1 = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
          else
            Output1 = InputB + InputA;
          end
          Output2 = 0;
          Error1=0;
          Error2=0;
        case 'ResetTrue'
          if J.DiscardReset
            [~, Output2] = J.monteCarlo(InputB, InputA, J.RatioORB2, J.RatioORA1);
          else
            Output2 = InputB + InputA;
          end
          Output1 = 0;
          Error1=0;
          Error2=0;
        otherwise % Pass junction is the default
          [Output1, Output2, Error1, Error2] = J.monteCarlo(InputB, InputA, J.ErrorOPB, J.ErrorOP);
      end
    end
    
    
    function Register=calculateLineSegment(J,Register, ind, JunctionType)
      %calculateLineSegment calculates the behavior of agents for the
      %current part of a line of a subset sum network.
      %
      % See also: SubSumNetworkClass.simulate
      
      n = ind * 2 - 1;
      [one, two, e1, e2] = J.calculate(Register(3, n), Register(3, n + 1), JunctionType);
      Register(4, n) = Register(4, n) + one + e1;
      Register(4, n + 3) = Register(4, n + 3) + two + e2;
      [one, two, e1, e2] = J.calculate(Register(1, n), Register(1, n + 1), JunctionType);
      Register(2, n) = Register(2, n) + one;
      Register(2, n + 3) = Register(2, n + 3) + two;
      Register(4, n) = Register(4, n) + e1;
      Register(4, n + 3) = Register(4, n + 3) + e2;
    end
    
    
  end
  methods (Static)
    
    
    function [Output1, Output2, Error1, Error2] = monteCarlo(InputB, InputA, TurningRateB, TurningRateA) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %monteCarlo randomly distributes agents from inputA and inputB to
      %output1 and output2 depending on  and TurningRateA.
      %On average, the ratio with which the agents are distributed is
      %determined by the TurningRateA for inputA and TurningRateB for inputB.
      %
      % See also: calculate
      
      A=rand(InputA,1);
      B=rand(InputB,1);
      A(A<TurningRateA)=[];
      B(B<TurningRateB)=[];
      Output1=length(B);
      Output2=length(A);
      Error1=(InputA-length(A));
      Error2=(InputB-length(B));
    end
    
    
    function Register = createLineRegister(ProblemSize)
      %createLineRegister create a register of the appropriate dimension
      %and type for this junction class.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register = zeros(4, (ProblemSize + 1) * 2, 'uint32');
    end
    
    
    function Register = shiftLineRegister(Register)
      %shiftLineRegister after processing a line is complite, this function
      %shifts the lines of a register sucht that processing of the next
      %line can begin.
      %
      % See also: SubSumNetworkClass.createJunction
      
      Register(1,:)=Register(2,:);
      Register(2,:)=0;
      Register(3,:)=Register(4,:);
      Register(4,:)=0;
    end
    
    
    function Results = calculateResults(Register, ProblemSize)
      %calculateResults calculates the actual results from the register used for simulation.
      %
      % See also: simulate
      
      Results = zeros(2, ProblemSize);
      Results(1, :) = Register(1, 1:2:ProblemSize * 2) + Register(1, 2:2:ProblemSize * 2);
      Results(2, :) = Register(3, 1:2:ProblemSize * 2) + Register(3, 2:2:ProblemSize * 2);
    end
    
    
  end
end