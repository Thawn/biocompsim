function plotTimes(extrapolatedXC, extrapolatedComputingTime,XC, computingTime, XA,ActinTime,XM,MTTime,timeUnit)
if nargin<9
  timeUnit='s';
end
switch timeUnit
  case 'min'
    ylim=400;
  case 'h'
    ylim=20;
  otherwise
    ylim=20000;
end

% Create figure
figure1 = figure();

% Create axes
axes1 = axes('Parent',figure1,'TickDir','out','YLim',[0 ylim],'XLim',[0 55]);
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[0 35]);
% Uncomment the following line to preserve the Y-limits of the axes
% ylim(axes1,[0 20000]);
hold(axes1,'all');

% Create plot

plot(extrapolatedXC,extrapolatedComputingTime,'Parent',axes1,'Color',[1 0 0],...
  'DisplayName','tianhe-2 extrapolated');

% Create xlabel
xlabel('cardinality (number of primes)','FontSize',12);

% Create ylabel
ylabel(['computing time (' timeUnit ')'],'FontSize',12);

% Create plot
%plot(X2,Y2,'Parent',axes1,'Marker','square','LineStyle','none',...
%  'Color',[0 1 1],...
%  'DisplayName','computing time (the hard way) measured');

% Create multiple lines using matrix input to plot
plot(XC,computingTime,'Parent',axes1,'LineStyle','none',...
  'Marker','o','Color',[1 0 0],...
  'DisplayName','laptop computer measured');
plot(XA,ActinTime,'Parent',axes1,'LineStyle','none',...
  'Marker','x','Color',[0 0 1],...
  'DisplayName','actin travel time');
plot(XM,MTTime,'Parent',axes1,'LineStyle','none',...
  'Marker','.','Color',[0 0 0],...
  'DisplayName','microtubule travel time');


% Create legend
legend1 = legend(axes1,'show','Location','Best'); %#ok<NASGU>
%set(legend1,...
%  'Position',[0.151866883116883 0.686851139444632 0.471590909090909 0.239442567567568]);
set(gcf,'Units','Inches');
pos = get(gcf,'Position');
set(gcf,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
saveas(gcf,['results' filesep 'plotTimes(' datestr(clock,'yyyymmddTHHMMSS') ').pdf']);
