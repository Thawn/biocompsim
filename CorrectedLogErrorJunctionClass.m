classdef CorrectedLogErrorJunctionClass < LogErrorJunctionClass
  %CorrectedJunctionClass class used to simulate the behavior of agents at
  %individual junctions within a subset sum network. Takes additional
  %parameters to try and adjust the splitting ratios to correct for the
  %fact that the chance of an exit in the center to be accessible by more
  %than one solution is higher than at the edges. That way, filaments are
  %spread relatively evenly throughout the network. Keeps track of how
  %many agents have made one or more errors and how many agents behaved
  %correctly.
  %
  % See also: SubSumNetworkClass,DeterministicJunctionClass,LogErrorJunctionClass,CorrectedJunctionClass,CorrectedLogErrorJunctionClass
  
  methods
    function J=CorrectedLogErrorJunctionClass(varargin)
      J@LogErrorJunctionClass(varargin{:})
    end
    function [output1, output2, error1, error2]=calculate(J,inputB,inputA) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate randomly distributes agents from inputA and inputB to output1 and output2.
      %On average, the ratio with which the agents are distributed is
      %determined by the ErrorRate properties of the JunctionClass object.
      %Allows dynamic adjustment of splitting ratios.
      %Keeps track of how many agents have made one or more errors and how
      %many agents behaved correctly.
      %
      % See also: calculateLine
      
      A=rand(inputA,1);
      B=rand(inputB,1);
      A(A<J.ErrorRateA2)=[];
      B(B<J.ErrorRateB1)=[];
      if strcmp(J.JunctionType,'Pass')
        output1=length(B);
        output2=length(A);
        error1=(inputA-length(A));
        error2=(inputB-length(B));
      else
        output1=length(B)+(inputA-length(A));
        output2=length(A)+(inputB-length(B));
        error1=0;
        error2=0;
      end
    end
    function [register, errRegister]=calculateCorrectedLineErr(J,register,errRegister,lineWidth,splittingRatios)
      %calculateLine calculates the behavior of agents for an entire line
      %of a subset sum network. Allows dynamic adjustment of splitting
      %ratios to correct for the fact that the chance of an exit in the
      %center to be accessible by more than one solution is higher than at
      %the edges. That way, filaments are spread relatively evenly
      %throughout the network.
      %Keeps track of how many agents have made one or more errors and how
      %many agents behaved correctly.
      %
      % See also: SubSumNetworkClass.simulate
      
      if nargin<5
        splittingRatios(1:2:length(register(1,:))-1)=J.ErrorRateA2;
        splittingRatios(2:2:length(register(1,:)))=J.ErrorRateB1;
      end
      if size(register)~=size(errRegister)
        error('BioCompSim:matrixSize', 'The dimensions of the register (%d) and the error register (%d) must be identical',size(register),size(errRegister));
      end
      for i=1:2:lineWidth*2
        [one, two]=J.calculate(errRegister(1,i),errRegister(1,i+1),splittingRatios(i),splittingRatios(i+1));
        errRegister(2,i)=errRegister(2,i)+one;
        errRegister(2,i+3)=errRegister(2,i+3)+two;
        [one, two]=J.calculate(register(1,i),register(1,i+1),splittingRatios(i),splittingRatios(i+1));
        register(2,i)=register(2,i)+one;
        register(2,i+3)=register(2,i+3)+two;
      end
      register(1,:)=register(2,:);
      register(2,:)=0;
      errRegister(1,:)=errRegister(2,:);
      errRegister(2,:)=0;
    end
  end
end