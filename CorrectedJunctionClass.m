classdef CorrectedJunctionClass < JunctionClass
  %CorrectedJunctionClass class used to simulate the behavior of agents at
  %individual junctions within a subset sum network. Takes additional
  %parameters to try and adjust the splitting ratios to correct for the
  %fact that the chance of an exit in the center to be accessible by more
  %than one solution is higher than at the edges. That way, filaments are
  %spread relatively evenly throughout the network.
  %
  % See also: SubSumNetworkClass,DeterministicJunctionClass,LogErrorJunctionClass,JunctionClass,CorrectedLogErrorJunctionClass
  
  methods
    function J=CorrectedJunctionClass(varargin)
      J@JunctionClass(varargin{:})
    end
    function [Output1, Output2]=calculate(J,InputB,InputA,ErrB1,ErrA2) %inputB is read first here, because logically, B and 1 is the path that goes straight down and is placed first in the register
      %calculate randomly distributes agents from inputA and inputB to output1 and output2.
      %On average, the ratio with which the agents are distributed is
      %determined by the ErrorRate properties of the JunctionClass object.
      %Allows dynamic adjustment of splitting ratios.
      %
      % See also: calculateLine
      
      if nargin<4
        ErrA2=J.ErrorRateA2;
        ErrB1=J.ErrorRateB1;
      end
      A=rand(InputA,1);
      B=rand(InputB,1);
      A(A<ErrA2)=[];
      B(B<ErrB1)=[];
      Output1=length(B)+(InputA-length(A));
      Output2=length(A)+(InputB-length(B));
    end
    function Register=calculateLine(J,Register,LineWidth,SplittingRatios)
      %calculateLine calculates the behavior of agents for an entire line
      %of a subset sum network. Allows dynamic adjustment of splitting
      %ratios to correct for the fact that the chance of an exit in the
      %center to be accessible by more than one solution is higher than at
      %the edges. That way, filaments are spread relatively evenly
      %throughout the network.
      %
      % See also: SubSumNetworkClass.simulate
      
      if nargin<4
        SplittingRatios(1:2:length(Register(1,:))-1)=J.ErrorRateA2;
        SplittingRatios(2:2:length(Register(1,:)))=J.ErrorRateB1;
      end
      for i=1:2:LineWidth*2
        [one, two]=J.calculate(Register(1,i),Register(1,i+1),SplittingRatios(i),SplittingRatios(i+1));
        Register(2,i)=Register(2,i)+one;
        Register(2,i+3)=Register(2,i+3)+two;
      end
      Register(1,:)=Register(2,:);
      Register(2,:)=0;
    end
  end
end