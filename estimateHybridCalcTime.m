function [time,timeBio,timeComp,numbersComp,numbersBio] = estimateHybridCalcTime(numbers,diagonalUnitLength,filamentSpeed,timeUnit)
%estimateHybridCalcTime estimate how long a hybrid calculation would take
%if the first 24 numbers are calculated by a computer and the results were
%fed into a subset sum network encoding the remaining numbers.

if nargin<1
  numbers=randi(1000,1,40);
end
if nargin<2
  diagonalUnitLength=5.415; %for the actin device, for the MT device it is 10.6398
end
if nargin<3
  filamentSpeed=5; %in �m/s; default is set for actin
end
if nargin<4
  timeUnit='s';
end
switch timeUnit
  case 'm'
    timeCor=60;
  case 'h'
    timeCor=3600;
  otherwise
    timeCor=1;
end
numbers=sort(numbers);
cardinality=length(numbers);
if cardinality>25
  numbersComp=numbers(cardinality-24:end);
  numbersBio=numbers(1:cardinality-25);
  timesbio=estimateCalculationTime(numbersBio,diagonalUnitLength,filamentSpeed);
  timeBio=timesbio(end)/timeCor;
else
  timeBio=0;
  numbersBio=[];
  numbersComp=numbers;
end
tic;
subSumTheHardWay(numbersComp);
timeComp=toc/timeCor;
time=timeComp+timeBio;
