function string=ellipsize(string,maxLength)
%ellipsize shortens string if it exceeds the integer maxLength. Does not
%shorten from the end but cuts out part from the middle and replaces it
%with two or three dots. Used to keep filenames manageable.

if maxLength > 100 && ispc %hack to fix too long filenames for windows
    maxLength = 100;
end

if length(string)>maxLength
  [~,string,~]=fileparts(string);
  if length(string)>maxLength
    halfLength=floor(maxLength/2-1);
    string=[string(1:halfLength),repmat('.',1,maxLength-(2*halfLength)),string(end-halfLength+1:end)];
  end
end
